
# setup julia in mac12

/Applications/Julia-1.2.app/Contents/Resources/julia/bin/julia
project_folder = "/Users/nbonacker/Projects/01_inverse_problem/"
cd( project_folder)



# setup julia in Niklas-ThinkPad-L13-Yoga

/home/niklas/Source/julia-1.4.1/bin/julia
project_folder = "/home/niklas/Projects/01_inverse_problem/"
cd( project_folder)



# use packages

using Revise
includet( project_folder * "/julia/simulation.jl")
using .Simulation
includet( project_folder * "/julia/gaussian_theory.jl")
using .GaussianTheory
includet( project_folder * "/julia/mean_field_theory.jl")
using .MeanFieldTheory
includet( project_folder * "/julia/network_inference.jl")
using .NetworkInference # for the function get_errors_averaged
includet( project_folder * "/julia/plot_results.jl")
using .PlotResults
using Plots, ColorSchemes, JLD2



# generate scatter plots of the inverse problem without parameter optimization

no_nodes = 10
no_pertubations = 10
no_drugs = 10
pertubation_type = "single"
matrix_type = "randn"
β = 0.5
a = ones( no_nodes)
b = ones( no_nodes)
θ = zeros( no_nodes)

for c in [ 0.1 * ones( no_nodes) , 1.0 * ones( no_nodes)]

	ω, pert, x = simulate_data( no_nodes, no_pertubations, no_drugs, pertubation_type, matrix_type, β, a, b, c, θ)

	for no_samples_per_pertubation in [ 5, 50]

		samples = get_samples( x, no_samples_per_pertubation)
		 ω_start = zeros( no_nodes, no_nodes)
		 search_space = Dict( "lb" => -1.0 * ones( no_nodes^2), "ub" => 1.0 * ones( no_nodes^2))

		@time ω_ms_1o = min_squares( pert, samples, θ, "exact relations first order", :LN_BOBYQA, "flat prior", a, b, c, 1e-2, 2e+4, vec( ω_start), search_space)
		println( "ω_ms_1o for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
		println()

		@time ω_ms_2o = min_squares( pert, samples, θ, "exact relations second order", :LN_BOBYQA, "flat prior", a, b, c, 1e-2, 2e+4, vec( ω_start), search_space)
		println( "ω_ms_2o for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
		println()

		@time ω_ms_gt = min_squares( pert, samples, θ, "gaussian theory", :LN_BOBYQA, "flat prior", a, b, c, 1e-2, 2e+4, vec( ω_start), search_space)
		println( "ω_ms_gt for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
		println()

		@time ω_ml_gt = max_likelihood( pert, samples, θ, "gaussian theory", :LN_BOBYQA, "flat prior", a, b, c, 1e-2, 2e+4, vec( ω_start), search_space)
		println( "ω_ml_gt for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
		println()

		color_scheme = ColorSchemes.rainbow
		color_list_1 = [ color_scheme[ 1/4], color_scheme[ 2/4]]
		color_list_2 = [ color_scheme[ 3/4], color_scheme[ 4/4]]
		x_data = ω
		y_data_1 = [ ω_ms_1o, ω_ms_2o]
		y_data_2 = [ ω_ms_gt, ω_ml_gt]
		x_label =  "generated interactions"
		y_label =  "reconstructed interactions"
		x_lim = ( -1.0, 1.0)
		y_lim = ( -1.0, 1.0)
		x_ticks = ( [ -1.0, -0.5, 0.0, 0.5, 1.0], [ "-1.0", "-0.5", "0.0", "0.5", "1.0"])
		y_ticks = ( [ -1.0, -0.5, 0.0, 0.5, 1.0], [ "-1.0", "-0.5", "0.0", "0.5", "1.0"])
		output_1_pdf = project_folder * "plots/inverse_problem/scatter_ω_β=" * string(β) * "_c=" * string(c[1]) * "_no_samples_per_perturbation=" * string( no_samples_per_pertubation) * "_1.pdf"
		output_2_pdf = project_folder * "plots/inverse_problem/scatter_ω_β=" * string(β) * "_c=" * string(c[1]) * "_no_samples_per_perturbation=" * string( no_samples_per_pertubation) * "_2.pdf"
		scatter_forward_problem( x_data, y_data_1, x_label, y_label, x_lim, y_lim, x_ticks, y_ticks, color_list_1, output_1_pdf)
		scatter_forward_problem( x_data, y_data_2, x_label, y_label, x_lim, y_lim, x_ticks, y_ticks, color_list_2, output_2_pdf)
	end
end

output_txt = project_folder * "plots/inverse_problem/scatter_β=" * string(β) * "_no_nodes=" * string( no_nodes) * ".txt"

io = open( project_folder * "scripts/scatter_plot_inverse_problem.txt", "r")
	foo = read( io, String)
close( io)

io = open( output_txt, "w")
	write( io, foo)
close( io)
