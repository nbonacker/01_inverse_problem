
## Data Search

### Morgan2019

>Perturbation-based gene regulatory network inference to unravel oncogenic mechanisms

[Link paper](https://www.biorxiv.org/content/10.1101/735514v2.full)

[Link data](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE125958)

[Link data set](https://www.ncbi.nlm.nih.gov/gds/?term=GSE125958[ACCN]%20AND%20gsm[ETYP])

do so far not understand the data

### Sander2013

> Perturbation Biology: Inferring Signaling Networks in Cellular Systems

[Link paper and data set](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003290#s4)

data folder Sander Sander2013
publication Molinelli 2013
empty file prior.txt
