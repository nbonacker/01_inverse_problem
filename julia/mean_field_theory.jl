
module MeanFieldTheory

	export get_mχ_mean_field_theory_first_order, get_mχ_mean_field_theory_second_order, get_mχ_mean_field_theory_first_order_alternative, get_mχ_mean_field_theory_second_order_alternative

	using LinearAlgebra

	# calculate m and χ for pertubation μ in first order mft

 	function get_mχ_mean_field_theory_first_order( ω, u, a, b, c)

		no_nodes = size( ω)[ 1]
		m = zeros( no_nodes)
		convergence = false
		Δ = 0.8
		runs = 0

		while !convergence

			m_update = get_m_update_mean_field_theory_first_order( ω, m, u, a, b)
			runs += 1
			convergence = get_convergence( m, m_update)
			m = Δ * m_update + ( 1 - Δ) * m

			if runs >= 2e+5

				println( "mean field theory first order : NoConvergence")
				break
			end
		end

		χ = Array{ Float64, 2}( undef, no_nodes, no_nodes)

		for i in 1:no_nodes, j in 1:no_nodes

			χ[ i, j]	= 0.5 * δ( i, j)
			χ[ i, j] += 0.5 * ( 1 - m[ i]^2) * ( 1 - m[ j]^2) * ω[ i, j]
			χ[ i, j] += 0.5 * ( 1 - m[ j]^2) * ( 1 - m[ i]^2) * ω[ j, i]
		end

		println( runs)

		return m, χ
	 end

	# get m in first order mft

	function get_m_update_mean_field_theory_first_order( ω, m, u, a, b)

		no_nodes = length( m)
		m_update = Array{ Float64}( undef, no_nodes)

		for i in 1:no_nodes

	 		foo = 0.0

	 		for k in 1:no_nodes

				foo += ω[ i, k] * m[ k]
	 		end

 			m_update[ i] = tanh( foo + u[ i])
		end

			return m_update
	end

	# calculate m and χ for pertubation μ in first order mft

	function get_mχ_mean_field_theory_first_order_alternative( ω, u, a, b, c)

		no_nodes = size( ω)[ 1]
		m = zeros( no_nodes)
		convergence = false
		Δ = 0.8
		runs = 0

		while !convergence

			m_update = get_m_update_mean_field_theory_first_order_alternative( ω, m, u, a, b)
			runs += 1
			convergence = get_convergence( m, m_update)
			m = Δ * m_update + ( 1 - Δ) * m

			if runs >= 2e+5

				println( "mean field theory first order alternative: NoConvergence")
				break

			end
		end

		χ = 0.5 * Matrix( I, no_nodes, no_nodes)

		for i in 1:no_nodes, j in 1:no_nodes

			foo = 0.25 * ( 1 - m[ i]^2) * ω[ i, j]
			bar = 0.25 * ( 1 - m[ j]^2) * ω[ j, i]
			χ[ i, j] += foo + bar
		end

		return m, χ
	 end

	# get m in first order mft

	function get_m_update_mean_field_theory_first_order_alternative( ω, m, u, a, b)

		no_nodes = length( m)
		m_update = Array{ Float64}( undef, no_nodes)

		for i in 1:no_nodes

			foo = 0.0

			for k in 1:no_nodes

				foo += ω[ i, k] * m[ k]
			end

			m_update[ i] = tanh( foo + u[ i])
		end

		return m_update
	end

	# calculate m and χ for pertubation μ in second order mft

	function get_mχ_mean_field_theory_second_order( ω, u, a, b, c)

		no_nodes = size( ω)[ 1]
		m = zeros( no_nodes)
		convergence = false
		Δ = 0.8
		runs = 0

		while !convergence

			m_update = get_m_update_mean_field_theory_second_order( ω, m, u, a, b, c)
			runs += 1
			convergence = get_convergence( m, m_update)
			m = Δ * m_update + ( 1 - Δ) * m

			if runs >= 2e+5

				println( "mean field theory second order : NoConvergence")
				break
			end
		end

		χ = Array{ Float64, 2}( undef, no_nodes, no_nodes)
		ω_sym = 0.5 * ( ω + transpose( ω))

		for i in 1:no_nodes, j in 1:no_nodes

			α_ij = 0.0
			α_ji = 0.0

			for k in 1:no_nodes

				if k == i

					continue
				end

				α_ij += ω[ j, k] * ω_sym[ i, k] * ( 1 - m[ k]^2)
				α_ji += ω[ i, k] * ω_sym[ j, k] * ( 1 - m[ k]^2)
			end

			β_ij = 2 * m[ i] * m[ j] * ω[ j, i]^2
			β_ji = 2 * m[ j] * m[ i] * ω[ i, j]^2

			χ[ i, j] = 0.5 * δ( i, j)
			χ[ i, j] += 0.5 * ( 1 - m[ i]^2) * ( 1 - m[ j]^2) * ( ω[ i, j] + α_ij + β_ij)
			χ[ i, j] += 0.5 * ( 1 - m[ j]^2) * ( 1 - m[ i]^2) * ( ω[ j, i] + α_ji + β_ji)
		end

		return m, χ
	end

	# get m in second order mft

	function get_m_update_mean_field_theory_second_order( ω, m, u, a, b, c)

		no_nodes = length( m)
		m_update = Array{ Float64}( undef, no_nodes)

		for i in 1:no_nodes

	 		foo = 0.0

	 		for j in 1:no_nodes

				foo += ω[ i, j] * m[ j] - m[ i] * ω[ i, j]^2 * ( 1 - m[ j]^2)
	 		end

 			m_update[ i] = tanh( foo + u[ i])
		end

		return m_update
	end

	# calculate m and χ for pertubation μ in second order mft

	function get_mχ_mean_field_theory_second_order_alternative( ω, u, a, b, c)

		no_nodes = size( ω)[ 1]
		m = zeros( no_nodes)
		convergence = false
		Δ = 0.8
		runs = 0

		while !convergence

			m_update = get_m_update_mean_field_theory_second_order_alternative( ω, m, u, a, b, c)
			runs += 1
			convergence = get_convergence( m, m_update)
			m = Δ * m_update + ( 1 - Δ) * m

			if runs >= 2e+5

				println( "mean field theory second order alternative: NoConvergence")
				break
			end
		end

		θ_MFT1 = zeros( no_nodes)
		# θ_MFT2 = zeros( no_nodes)

 		for i in 1:no_nodes

			for j in 1:no_nodes

				θ_MFT1[ i] += - ω[ i, j] * m[ j]
				# θ_MFT2[ i] += - ω[ i, j] * m[ j] + 0.5 * m[ i] * ω[ i, j]^2
			end

			# θ_MFT2[ i] = θ_MFT1[ i] # use workaround to plug in the mft expansion only first second order expressions in θ and ω
		end

		χ = 0.5 * Matrix( I, no_nodes, no_nodes)

		for i in 1:no_nodes, j in 1:no_nodes

			# mft1

			foo = 0.25 * ( 1 - m[ i]^2) * ω[ i, j]
			bar = 0.25 * ( 1 - m[ j]^2) * ω[ j, i]

			# mft2

			foo += - 0.5 * ( 1 - m[ j]^2) * m[ j] * θ_MFT1[ j] * ω[ j, i]
			bar += - 0.5 * ( 1 - m[ i]^2) * m[ i] * θ_MFT1[ i] * ω[ i, j]

			for k in 1:no_nodes

				foo += 0.125 * ( 1 - m[ i]^2) * ( 1 - m[ j]^2) * ω[ i, k] * ω[ j, k] + 0.125 * ( 1 - m[ j]^2) * ω[ j, k] * ( 1 - m[ k]^2) * ω[ k, i] + m[ i] * m[ j] / 12 * ( 1 - m[ j]^2) * ω[ j, k]^2
				bar += 0.125 * ( 1 - m[ j]^2) * ( 1 - m[ i]^2) * ω[ j, k] * ω[ i, k] + 0.125 * ( 1 - m[ i]^2) * ω[ i, k] * ( 1 - m[ k]^2) * ω[ k, j] + m[ j] * m[ i] / 12 * ( 1 - m[ i]^2) * ω[ i, k]^2
			end

			foo += - m[ i] * m[ j] / 6 * ( 1 - m[ j]^2) * ω[ j, i]^2 + 1 / 3 * m[ j] * ( 1 - m[ j]^2) * ω[ j, i] * θ_MFT1[ j]
			bar += - m[ j] * m[ i] / 6 * ( 1 - m[ i]^2) * ω[ i, j]^2 + 1 / 3 * m[ i] * ( 1 - m[ i]^2) * ω[ i, j] * θ_MFT1[ i]

			χ[ i, j] += ( foo + bar)
		end

		return m, χ
	end

	# get m in second order mft

	function get_m_update_mean_field_theory_second_order_alternative( ω, m, u, a, b, c)

		no_nodes = length( m)
		m_update = Array{ Float64}( undef, no_nodes)

		for i in 1:no_nodes

			foo = 0.0

			for j in 1:no_nodes

				foo += ω[ i, j] * m[ j] - 0.5 * m[ i] * ω[ i, j]^2
			end

			m_update[ i] = tanh( foo + u[ i])
		end

		return m_update
	end

	# get m and χ for pertubation μ in second order mft

	function get_mχ_mean_field_theory_third_order( ω, u, a, b, c)

		no_nodes = size( ω)[ 1]
		m = zeros( no_nodes)
		convergence = false
		Δ = 0.8
		runs = 0

		while !convergence

			foo = update_m_mean_field_theory_third_order( ω, m, u, a, b, c)
			runs += 1
			convergence = get_convergence( m, foo)
			m = Δ * foo + ( 1 - Δ) * m

			if runs >= 1e+4

				println( "mean field theory third order : NoConvergence")
				break
			end
		end

		χ = Array{ Float64, 2}( undef, no_nodes, no_nodes)
		ω_sym = 0.5 * ( ω + transpose( ω))

		for i in 1:no_nodes, j in 1:no_nodes

			α_ij = 0.0
			α_ji = 0.0

			for k in 1:no_nodes

				α_ij += ω[ j, k] * ω_sym[ i, k] * ( 1 - m[ k]^2)
				α_ji += ω[ i, k] * ω_sym[ j, k] * ( 1 - m[ k]^2)
			end

			β_ij = 2 * m[ i] * m[ j] * ω[ j, i]^2
			β_ji = 2 * m[ j] * m[ i] * ω[ i, j]^2

			χ[ i, j] = 0.5 * δ( i, j) + 0.5 * ( 1 - m[ i]^2) * ( 1 - m[ j]^2) * ( ω[ i, j] + α_ij + β_ij)
			χ[ i, j] = χ[ i, j] + 0.5 * ( 1 - m[ j]^2) * ( 1 - m[ i]^2) * ( ω[ j, i] + α_ji + β_ji)
		end

		for i in 1:no_nodes, j in 1:no_nodes

			α_ij = 0.0
			α_ji = 0.0

			for k in 1:no_nodes

				α_ij += ω[ j, k] * ω_sym[ i, k] * ( 1 - ( b[ k] / a[ k] * m[ k])^2)
				α_ji += ω[ i, k] * ω_sym[ j, k] * ( 1 - ( b[ k] / a[ k] * m[ k])^2)
			end

			β_ij = 2 * m[ i] * m[ j] * ω[ j, i]^2
			β_ji = 2 * m[ j] * m[ i] * ω[ i, j]^2

			χ[ i, j] = c[ i]^2 / ( b[ i] + b[ j]) * δ( i, j)
			χ[ i, j] += 0.5 * ( 1 - ( b[ i] / a[ i] * m[ i])^2) * ( 1 - ( b[ j] / a[ j] * m[ j])^2) * ( ω[ i, j] + α_ij + β_ij)
			χ[ i, j] += 0.5 * ( 1 - ( b[ j] / a[ j] * m[ j])^2) * ( 1 - ( b[ i] / a[ i] * m[ i])^2) * ( ω[ j, i] + α_ji + β_ji)
		end

		return m, χ
	end

	# get m in second order mft

	function update_m_mean_field_theory_third_order( ω, m, u, a, b, c)

		no_nodes = size( ω)[ 1]
		m = Array{Float64}( undef, no_nodes)

		for i in 1:no_nodes

			a_i = 0.0

			for j in 1:no_nodes

				a_i += ω[ i, j]^2 * ( 1 - m[ j]^2)
			end

			b_i = 0.0

			for j in 1:no_nodes

				b_i += ω[ i, j]^3 * ( 1 - m[ j]^2) * m[ j]
			end

			c_i = 0.0

			for j in 1:no_nodes, k in 1:no_nodes

				if j == k

					continue
				end
			end
		end

		return foo
	end

	# check for convergence, wih diff = 0.01

	function get_convergence( a, b)

		no_elements = length( a)
		diff = 0.01

		for i in 1:no_elements

			if abs( a[ i] - b[i]) > diff

				return false
			end
		end

		return true
	end

	# define kronecker δ

	function δ( i, j)

		if i == j

			return 1
		else

			return 0
		end
	end
end
