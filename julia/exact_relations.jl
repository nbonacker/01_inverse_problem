
module ExactRelations

	export get_m_exact_relations, get_C_exact_relations, get_Ω_exact_relations, get_mC_exact_relations, get_∂m_∂C_∂χ_exact_relations

	using Statistics

	# calculate the mean m and the correlation C given interaction ω and pertubation u

	function get_mC_exact_relations( samples, ω, u, a, b, c)

		m = get_m_exact_relations( samples, ω, u, a, b)
		C = get_C_exact_relations( samples, ω, u, a, b, c)

			return m, C
	end

	# calculate mean vector m with exact relations

	function get_m_exact_relations( samples, ω, u, a, b)

		no_nodes = size( samples)[1]
		no_samples = size( samples)[ 2]

		m = Array{Float64}( undef, no_nodes)

		for i in 1:no_nodes

			foo = Array{Float64}( undef, no_samples)

				for k in 1:no_samples

					foo[k] = tanh( transpose( ω[ i, :]) * samples[ :, k] + u[ i])
				end

			m[ i] = a[ i] / b[ i] * mean( foo)
		end

		return m
	end

	# calculate the correlation matrix C with exact relations

	function get_C_exact_relations( samples, ω, u, a, b, c)

		no_nodes = size( samples)[1]
		no_samples = size( samples)[ 2]

		C = Array{Float64}( undef, no_nodes, no_nodes)

		for i in 1:no_nodes, j in 1:i

			foo = Array{Float64}( undef, no_samples)

			for k in 1:no_samples

				foo[k] = a[ i] / ( b[ i] + b[ j]) * tanh( transpose( ω[ i, :]) * samples[ :, k] + u[ i]) * samples[ j, k] + a[ j] / ( b[ i] + b[ j]) * tanh( transpose( ω[ j, :]) * samples[ :, k] + u[ j]) * samples[ i, k] + c[ i]^2 / ( b[ i] + b[ j]) * δ( i, j)
			end

			C[ i, j] = C[ j, i] = mean( foo)
		end

		return C
	end

	# calculate the three point correlation matrix Ω with exact relations

	function get_Ω_exact_relations( samples, ω, u, a, b, c)

		no_nodes = size( samples)[1]
		no_samples = size( samples)[ 2]

		Ω = Array{Float64}( undef, no_nodes, no_nodes, no_nodes)

		for i in 1:no_nodes, j in 1:i, k in 1:j

			foo = Array{Float64}( undef, no_samples)

			for s in 1:no_samples

				foo[s] = ( b[ i] + b[ j] + b[ k])^( -1) * ( a[ i] * tanh( transpose( ω[ i, :]) * samples[ :, s] + u[ i]) * samples[ j, s] * samples[ k, s] + tanh( transpose( ω[ j, :]) * samples[ :, s] + u[ j]) * samples[ k, s] * samples[ i, s] + tanh( transpose( ω[ k, :]) * samples[ :, s] + u[ k]) * samples[ i, s] * samples[ j, s] + c[ i]^2 * δ( i, j) * samples[ k, s] + c[ j]^2 * δ( j, k) * samples[ i, s] + c[ k]^2 * δ( k, i) * samples[ j, s])
			end

			Ω[ i, j, k] = Ω[ j, k, i] = Ω[ k, i, j] = Ω[ j, i, k] = Ω[ i, k, j] = Ω[ k, j, i] = mean( foo)
		end

		return Ω
	end

	# calculate the derivative ∂m and ∂χ

	function get_∂m_∂C_∂χ_exact_relations( samples, ω, u, m, a, b, c)

		no_nodes = size( samples)[1]
		no_samples = size( samples)[ 2]
		∂m = zeros( no_nodes, no_nodes, no_nodes)
		∂C = zeros( no_nodes, no_nodes, no_nodes, no_nodes)

		for i in 1:no_nodes, w in 1:no_nodes, n in 1:no_nodes # contribution only for i = w

			for s in 1:no_samples

				∂m[ i, w, n] += a[ i] / b[ i] * ( 1 - tanh( transpose( ω[ i, :]) * samples[ :, s] + u[ i])^2) * samples[ n, s] * δ( i, w)
			end

			for j in 1:no_nodes, s in 1:no_samples

				∂C[ i, j, w, n] += a[ i] / ( b[ i] + b[ j]) * ( 1 - tanh( transpose( ω[ i, :]) * samples[ :, s] + u[ i])^2) * samples[ j, s] * samples[ n, s] * δ( i, w)
				∂C[ j, i, w, n] += a[ j] / ( b[ i] + b[ j]) * ( 1 - tanh( transpose( ω[ i, :]) * samples[ :, s] + u[ i])^2) * samples[ j, s] * samples[ n, s] * δ( i, w)
			end
		end

		∂m = ∂m ./ no_samples
		∂C = ∂C ./ no_samples
		∂χ = get_∂χ( ∂C, ∂m, m)

		return ∂m, ∂C, ∂χ
	end

	# define kronecker δ

	function δ( i::Int64, j::Int64)

		if i == j

			return 1
		else

			return 0
		end
	end
end
