
module Simulation

	export generate_single_pert, generate_double_pert, generate_randn_pert, simulate_data, plot_time_evolution, get_m_simulation, get_C_simulation, get_Ω_simulation, get_χ_simulation, get_samples, get_C, get_error_m, get_error_χ

	using Distributions

	# simulate the data in silico

	function simulate_data( no_nodes, no_pertubations, no_drugs, pertubation_type, matrix_type, β, a, b, c, θ)

		no_time_steps = Int( 1e+5)

		if matrix_type == "randn"

			ω = generate_randn_ω( no_nodes, β)
		elseif matrix_type == "sparse"

			ω = generate_sparse_ω( no_nodes, β)
		else

			throw( NoMatrixType)
		end

		if pertubation_type == "single"

			pert = generate_single_pert( no_nodes, no_pertubations)
		elseif pertubation_type == "randn"

			pert = generate_randn_pert( no_nodes, no_pertubations)
		elseif pertubation_type == "zeros"

			pert = zeros( no_nodes, no_pertubations)
		elseif pertubation_type == "double"

			pert = generate_double_pert( no_nodes, no_pertubations, no_drugs)
		else
			throw( NoPertType)
		end

		x = [ Array{ Float64, 2}( undef, no_nodes, no_time_steps) for idx in 1:no_pertubations]
		no_time_steps_pre_run = Int( 1e+5)

		for μ in 1:no_pertubations

			θ_pert = θ + pert[ :, μ]
			x[ μ] = get_x( ω, θ_pert, a, b, c, no_time_steps, no_time_steps_pre_run)
		end

		return ω, pert, x
	end

	# generate random interaction matrix

	function generate_randn_ω( no_nodes, β)

		ω = zeros( no_nodes, no_nodes)

		# used : variance = 1/N [1103.3433] p. 2
		# not used : variance = 1/sqrt(N) [PhysRevE.61.5658] p. 5660
		# Normal( μ, σ)
		# The normal distribution is parameterized by its mean μ and standard deviation σ

		distribution = Normal( 0, sqrt( 1/no_nodes))

		for i = 1:no_nodes, j = 1:no_nodes

			if i == j

				continue
			end

			ξ = β * rand( distribution)
			ω[ i, j] = ξ
		end

		return ω
	end

	# generate sparse interaction matrix

	function generate_sparse_ω( no_nodes, β)

		ω = zeros( no_nodes, no_nodes)
		n = 1

		while n <= 2.0 * no_nodes

			i, j = get_random_matrix_element( no_nodes)
			ω[ i, j] = β * ( rand() - 0.5) * 2
			n += 1
		end

		return ω
	end

	# get random non diagonal matrix element

	function get_random_matrix_element( no_nodes)

		i = rand( 1:no_nodes)
		j = rand( 1:no_nodes)

		if i == j

			return get_random_matrix_element( no_nodes)
		end
		return i, j
	end

	# generate orderd single pertubation

	function generate_single_pert( no_nodes, no_pertubations)

		pert = zeros( no_nodes, no_pertubations)

		for i in 1: no_pertubations

			pert[ (i-1)%no_nodes + 1, i] = - 2
		end

		return pert
	end

	# generate orderd double pertubation

	function generate_double_pert( no_nodes, no_pertubations, no_drugs)

		pert = zeros(no_nodes, no_pertubations)

		a = 1
		b = 1

		for i in 1:no_pertubations

			# println( "a: ", a)
			# println( "b: ", b)

			# pert[ Int(( floor( (i-1) / no_nodes)) % no_nodes + 1), i] = -1 every gene is can be knowed down
			# pert[ (i % no_nodes) + 1, i] = -2

			pert[ a, i] = -1
			pert[ b, i] = -2

			# println( "a: ", Int( ( a + 1) % no_drugs + 1))
			# println( "b: ", Int( floor( ( b + 1) / no_drugs) + 1))

			a = Int( ( a + 1) % no_drugs + 1)
			b = Int( floor( i / no_drugs) + 1)
		end

		return pert
	end

	# generate a random perturbation

	function generate_randn_pert( no_nodes, no_pertubations)

		distribution = Normal( 0, sqrt( 1/no_nodes))
		pert = rand( distribution, no_nodes, no_pertubations)

		return pert
	end

	# calculate the whole time evolution

	function get_x( ω, θ, a, b, c, no_time_steps, no_time_steps_pre_run)

		no_nodes = size( ω)[ 1]
		δt = 0.01
		x_time_step = 0.5 * randn( no_nodes)
		distribution = Normal( 0.0, sqrt( δt))
		x = Array{ Float64, 2}( undef, no_nodes, no_time_steps)

		for i in 1:no_time_steps_pre_run

			x_time_step = time_evolution_stochastic( δt, x_time_step, ω, θ, a, b, c, distribution)
		end

		for t in 1:no_time_steps

			x[ :, t] = x_time_step
			x_time_step = time_evolution_stochastic( δt, x_time_step, ω, θ, a, b, c, distribution)
		end

		return x
	end

	# calculate the whole time evolution

	function get_x_retarded( ω, θ, a, b, c, no_time_steps)

		no_nodes = size( ω)[ 1]
		δt = 0.01
		x_time_step = zeros( no_nodes)
		distribution = Normal( 0.0, sqrt( δt))
		x = Array{Float64}( no_nodes, no_time_steps)

		no_time_steps_pre_run = Int( 1e+5)

		x_pre_run = zeros( no_nodes, no_time_steps_pre_run)

		for t in 11:no_time_steps_pre_run

			x_pre_run[ :, t] = x_time_step
			x_time_step = time_evolution_stochastic( δt, x[ :, t - 10], ω, θ, a, b, c, distribution)
		end

		x[ :, 1:10] = x_pre_run[ :, ( no_time_steps_pre_run - 9) : no_time_steps_pre_run]

		for t in 11:no_time_steps

			x[ :, t] = x_time_step
			x_time_step = time_evolution_stochastic( δt, x[ :, t - 10], ω, θ, a, b, c, distribution)
		end

		return x
	end

	# calculate the next stochastic time step

	function time_evolution_stochastic( δt, x, ω, θ, a, b, c, distribution)

		return x + f( x, ω, θ, a, b) * δt + c .* rand( distribution, length(x))
	end

	# calculate the next deterministic time step

	function time_evolution_deterministic( δt, x, ω, θ, a, b, c)

		return x + f( x, ω, θ, a, b) * δt
	end

	# define function f = dx/dt

	function f( x, ω, θ, a, b)

		return a .* tanh.( ω * x + θ) - b .* x
	end

	# calculate the mean

	function get_m_simulation( x)

		no_pertubations = size( x)[ 1]
		no_nodes = size( x[ 1])[ 1]
		no_samples = size( x[ 1])[ 2]
		m_simulation = [ Array{Float64}( undef, no_nodes) for idx in 1:no_pertubations]

		for μ in 1:no_pertubations

			for i in 1:no_nodes

				m_simulation[ μ][ i] = mean( x[ μ][ i, :])
			end
		end

		return m_simulation
	end

	# calculate the correlation

	function get_C_simulation( x)

		no_pertubations = size( x)[ 1]
		no_nodes = size( x[ 1])[1]
		no_samples = size( x[ 1])[ 2]
		C_simulation = [ Array{Float64}( undef, no_nodes, no_nodes) for idx in 1:no_pertubations]

		for μ in 1:no_pertubations

			for i in 1:no_nodes, j in 1:i

				a = Array{Float64}( undef, no_samples)

				for t in 1:no_samples

					a[t] = x[ μ][ i, t]*x[ μ][ j, t]
				end

				C_simulation[ μ][ i, j] = C_simulation[ μ][ j, i] = mean(a)
			end
		end

		return C_simulation
	end

	# calculate the three point correlation

	function get_Ω_simulation( x)

		no_pertubations = size( x)[ 1]
		no_nodes = size( x[ 1])[1]
		no_samples = size( x[ 1])[ 2]
		Ω_simulation = [ Array{Float64}( undef, no_nodes, no_nodes, no_nodes) for idx in 1:no_pertubations]

		for μ in 1:no_pertubations

			for i in 1:no_nodes, j in 1:i, k in 1:j

				foo = Array{Float64}( undef, no_samples)

				for t in 1:no_samples

					foo[ t] = x[ μ][ i, t] * x[ μ][ j, t] * x[ μ][ k, t]
				end

				Ω_simulation[ μ][ i, j, k] = Ω_simulation[ μ][ j, k, i] = Ω_simulation[ μ][ k, i, j] = Ω_simulation[ μ][ j, i, k] = Ω_simulation[ μ][ i, k, j] = Ω_simulation[ μ][ k, j, i] = mean( foo)
			end
		end

		return Ω_simulation
	end

	# calculate the covariance

	function get_χ_simulation( x, m_simulation)

		no_pertubations = size( x)[ 1]
		no_nodes = size( x[ 1])[1]
		no_samples = size( x[ 1])[ 2]
		χ_simulation = [ Array{Float64}( undef, no_nodes, no_nodes) for idx in 1:no_pertubations]

		for μ in 1:no_pertubations

			for i in 1:no_nodes, j in 1:i

				a = Array{Float64}( undef, no_samples)

				for t in 1:no_samples

					a[t] = x[ μ][ i, t]*x[ μ][ j, t]
				end

				χ_simulation[ μ][ i, j] = χ_simulation[ μ][ j, i] = mean(a) - m_simulation[ μ][ i] * m_simulation[ μ][ j]
			end
		end

		return χ_simulation
	end

	# get data - some random samples - using the time dependent x

	function get_samples( x, no_random_samples)

		no_pertubations = size( x)[ 1]
		no_nodes = size( x[ 1])[ 1]
		no_time_steps = size( x[ 1])[ 2]
		samples = [ Array{Float64}( undef, no_nodes, no_random_samples) for idx in 1:no_pertubations]

		for μ in 1:no_pertubations

			for s in 1:no_random_samples

				samples[ μ][ :, s] = x[ μ][ :, Int( rand( 1:no_time_steps))]
			end
		end

		return samples
	end

	# get C given χ and m

	function get_C( χ, m)

		C = χ + m * transpose( m)

		return C
	end

	# get squared error of m_simulation and m

	function get_error_m( m_simulation, m)

		no_pert = length( m)
		no_nodes = length( m[ 1])
		foo = 0.0
		bar = 0.0

		for μ in 1:no_pert, i in 1:no_nodes

			foo += ( m_simulation[ μ][ i] - m[ μ][ i])^2
			bar += m_simulation[ μ][ i]^2
		end

		return sqrt( foo / bar)
	end

	# get squared error of χ_simulation and χ

	function get_error_χ( χ_simulation, χ)

		no_pert = length( χ)
		no_nodes = size( χ[ 1])[ 1]
		foo = 0.0
		bar = 0.0

		for μ in 1:no_pert, i in 1:no_nodes, j in 1:no_nodes

		foo += ( χ_simulation[ μ][ i, j] - χ[ μ][ i, j])^2
		bar += χ_simulation[ μ][ i, j]^2
		end

		return sqrt( foo / bar)
	end
end
