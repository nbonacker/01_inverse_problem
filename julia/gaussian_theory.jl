
module GaussianTheory

	export get_mχ_gaussian_theory

	using LinearAlgebra, QuadGK

	# calculate the mean m and the correlation C given interaction ω and pertubation u

	function get_mχ_gaussian_theory( ω, u, a, b, c)

		no_nodes = size( ω)[ 1]
		m, χ = zeros( no_nodes), Matrix{Float64}(I, no_nodes, no_nodes)
		convergence = false
		Δ = 0.8
		runs = 0

		while !convergence

			foo, bar = get_mχ( ω, χ, m, u, a, b, c)

			Δ_convercence = 1e-3
			convergence = get_convergence( m, foo, Δ_convercence) && get_convergence( χ, bar, Δ_convercence)

			m = Δ * foo + ( 1 - Δ) * m
			χ = Δ * bar + ( 1 - Δ) * χ

			if runs >= 1e+4

				#throw( NoConvergence)
				println( "gaussian theory: NoConvergence")
				return m, χ
			end

			runs += 1
		end

		# println( "runs gaussian theory : ", runs)
		return m, χ
	end

	# update correlation χ and mean m

	function get_mχ( ω, χ, m, u, a, b, c)

		# calculate g_μ and λ_μ

		# println( "ω = ", ω)
		# println( "m = ", m)
		# println( "u = ", u)

		g = get_g( ω, m, u)
		# println( "g = ", g)
		λ = get_λ( χ, g, ω)
		# println( "λ = ", λ)

		# println( "return m = ", get_m( χ, g, ω, a, b), " χ = ", get_χ( ω, χ, λ, a, b, c))

		return get_m( χ, g, ω, a, b), get_χ( ω, χ, λ, a, b, c)
	end

	# get g

	function get_g( ω, m, u)

		g = ω * m + u
		return g
	end

	# get λ_μ

	function get_λ( χ, g, ω)

		no_nodes = size( χ)[ 1]
		λ = Array{ Float64, 1}( undef, no_nodes)

		for i in 1:no_nodes

			Δ = get_Δ( χ, ω, i)
			# print( "get_λ i = ", i, " g[ i] = ", g[ i], " Δ = ", Δ)

			function inside_λ( x)

				return outside_λ( Δ, g[ i], x)
			end

			λ[ i] = 1 / sqrt( 2 * π * Δ) * QuadGK.quadgk( inside_λ, -10.0, 10.0)[ 1]
			# println( " λ[ i]: ", λ[ i])
		end

		return λ
	end

	# get Δ

	function get_Δ( χ, ω, i)

		no_nodes = size( ω)[ 1]
		Δ = 0.0

		for n in 1:no_nodes, m in 1:no_nodes

			Δ += ω[ i, n] * χ[ n, m] * ω[ i, m]
		end

		if Δ < 0.01

			return 0.01
		end

		return Δ
	end

	# calculate the integral kernel to get λ

	function outside_λ( Δ, g, x)

		return exp( - x^2 / ( 2 * Δ)) * ( 1 - tanh( g + x)^2)
	end

	# get m

	function get_m( χ, g, ω, a, b)

		no_nodes = size( χ)[ 1]
		m = Array{ Float64, 1}( undef, no_nodes)

		for i in 1:no_nodes

			Δ = get_Δ( χ, ω, i)

			if g[ i] == 0.0

					m[ i] = 0
					continue
			end

			function inside_m( x)

				return outside_m( Δ, g[ i], x)
			end

			m[ i] = a[ i] / b[ i] * 1 / sqrt( 2 * π * Δ) * QuadGK.quadgk( inside_m, -10.0, 10.0)[ 1]
		end

		return m
	end

	# calculate the integral kernel to get m_μ

	function outside_m( Δ, g, x)

		return exp( - x^2 / ( 2 * Δ)) * tanh( g + x)
	end

	# get χ

	function get_χ( ω, χ, λ, a, b, c)

		no_nodes = size( ω)[ 1]
		foo = Array{ Float64, 2}( undef, no_nodes, no_nodes)

		for i in 1:no_nodes, j in 1:i

			foo[ i, j] = foo[ j, i] = a[ j] / ( b[ i] + b[ j]) * transpose( χ[ i, :]) * ω[ j, :] * λ[ j] + a[ i] / ( b[ i] + b[ j]) * transpose( χ[ j, :]) * ω[ i, :] * λ[ i] + c[ i]^2 / ( 2 * b[ i]) * δ( i, j)
		end

		return foo
	end

	# get convergence of objects a and b

	function get_convergence( a, b, Δ = 1e-4)

		a_vec = vec( a)
		b_vec = vec( b)

		no_items = length( a_vec)

		for i in 1:no_items

			if abs( a_vec[ i] - b_vec[ i]) >= Δ

				return false
			end
		end

		return true
	end

	# calculate derivative of mean ∂m and the derivative of covariance ∂χ given interaction ω and pertubation u

	function get_∂m_∂χ_gaussian_theory( ω, u, m, χ, a, b, c)

		no_nodes = size( ω)[ 1]
		∂m, ∂χ = zeros( no_nodes, no_nodes, no_nodes), zeros( no_nodes, no_nodes, no_nodes, no_nodes)
		convergence = false
		Δ = 1.0
		runs = 0

		g = get_g( ω, m, u)
		λ = get_λ( χ, g, ω)
		κ = get_κ( χ, g, ω)
		γ = get_γ( χ, g, ω)
		ε = get_ε( χ, g, ω)

		while !convergence

			foo, bar = get_∂m_∂χ( ω, m, χ, g, λ, κ, γ, ε, ∂m, ∂χ, a, b, c)

			# println( "runs : ", runs)

			Δ_convergence = 1e-2
			convergence = get_convergence( ∂m, foo, Δ_convergence) && get_convergence( ∂χ, bar, Δ_convergence)

			∂m = Δ * foo + ( 1 - Δ) * ∂m
			∂χ = Δ * bar + ( 1 - Δ) * ∂χ

			if runs >= 4e+3

				#throw( NoConvergence)
				println( "gaussian theory partial derivative: NoConvergence")
				return ∂m, ∂χ
			end

			runs += 1
		end

		# println( "runs gradient gaussian theory :", runs)
		return ∂m, ∂χ
	end

	# update derivative of covariance ∂χ and derivative of mean ∂m

	function get_∂m_∂χ( ω, m, χ, g, λ, κ, γ, ε, ∂m, ∂χ, a, b, c)

		return get_∂m( ω, m, χ, λ, κ, ∂m, ∂χ, a, b, c), get_∂χ( ω, m, χ, g, λ, γ, ε, ∂χ, ∂m, a, b, c)
	end

	# get κ

	function get_κ( χ, g, ω)

		no_nodes = size( χ)[ 1]
		κ = Array{Float64}( no_nodes)

		for i in 1:no_nodes

			Δ = get_Δ( χ, ω, i)

			function inside_κ( x)

				return outside_κ( Δ, g[ i], x)
			end

			κ[ i] = QuadGK.quadgk( inside_κ, -10.0, 10.0)[ 1] / ( sqrt( 2 * π) * 2 * sqrt( Δ))
		end

		return κ
	end

	# calculate the integral kernel to get κ

	function outside_κ( Δ, g, x)

		return exp( - x^2 / 2) * ( 1 - tanh( g + sqrt( Δ) * x)^2) * x
	end

	# get ∂m

	function get_∂m( ω, m, χ, λ, κ, ∂m, ∂χ, a, b, c)

		no_nodes = size( ω)[ 1]
		∂m_return = zeros( no_nodes, no_nodes, no_nodes)

		for i in 1:no_nodes, n in 1:no_nodes, w in 1:no_nodes

			# if n == w continue end

			if i == n

				∂m_return[ i, n, w] += a[ i] / b[ i] * ( m[ w] * λ[ i] + κ[ i] * ( transpose( χ[ w, :]) * ω[ i, :] + transpose( ω[ i, :]) * χ[ :, w]))
			end

			∂m_return[ i, n, w] += a[ i] / b[ i] * ( λ[ i] * ( transpose( ω[ i, :]) * ∂m[ :, n, w]) + κ[ i] * transpose( ω[ i, :]) * ( ∂χ[ :, :, n, w] * ω[ i, :]))
		end

		return ∂m_return
	end

	# get ∂χ

	function get_∂χ( ω, m, χ, g, λ, γ, ε, ∂χ, ∂m, a, b, c)

		no_nodes = size( ω)[ 1]
		∂χ_return = zeros( no_nodes, no_nodes, no_nodes, no_nodes)

		for i in 1:no_nodes, j in 1:no_nodes, n in 1:no_nodes, w in 1:no_nodes

			if j == n

				∂χ_return[ i, j, n, w] += a[ j] / ( b[ i] + b[ j]) * χ[ i, w] * λ[ j] + a[ j] / ( b[ i] + b[ j]) * ( transpose( χ[ i, :]) * ω[ j, :]) * ( γ[ j] * m[ w] + ε[ j] * ( transpose( χ[ w, :]) * ω[ j, :] + transpose( ω[ j, :]) * χ[ :, w]))
			end

			if i == n

				∂χ_return[ i, j, n, w] += a[ i] / ( b[ i] + b[ j]) * χ[ j, w] * λ[ i] + a[ i] / ( b[ i] + b[ j]) * ( transpose( χ[ j, :]) * ω[ i, :]) * ( γ[ i] * m[ w] + ε[ i] * ( transpose( χ[ w, :]) * ω[ i, :] + transpose( ω[ i, :]) * χ[ :, w]))
			end

			∂χ_return[ i, j, n, w] += a[ j] / ( b[ i] + b[ j]) * transpose( ∂χ[ i, :, n, w]) * ω[ j, :] * λ[ j] + a[ j] / ( b[ i] + b[ j]) * transpose( χ[ i, :]) * ω[ j, :] * ( γ[ j] * transpose( ω[ j, :]) * ∂m[ :, n, w] + ε[ j] * transpose( ω[ j, :]) * ( ∂χ[ :, :, n, w] * ω[ j, :]))
			∂χ_return[ i, j, n, w] += a[ i] / ( b[ i] + b[ j]) * transpose( ∂χ[ j, :, n, w]) * ω[ i, :] * λ[ i] + a[ i] / ( b[ i] + b[ j]) * transpose( χ[ j, :]) * ω[ i, :] * ( γ[ i] * transpose( ω[ i, :]) * ∂m[ :, n, w] + ε[ i] * transpose( ω[ i, :]) * ( ∂χ[ :, :, n, w] * ω[ i, :]))
		end

		return ∂χ_return
	end

	# get γ

	function get_γ( χ, g, ω)

		no_nodes = size( χ)[ 1]
		γ = Array{Float64}( no_nodes)

		for i in 1:no_nodes

			Δ = get_Δ( χ, ω, i)

			function inside_γ( x)

				return outside_γ( Δ, g[ i], x)
			end

			γ[ i] = - sqrt( 2 / π) * QuadGK.quadgk( inside_γ, -10.0, 10.0)[ 1]
		end

		return γ
	end

	# calculate the integral kernel to get γ

	function outside_γ( Δ, g, x)

		return exp( - x^2 / 2) * tanh( g + sqrt( Δ) * x) * ( 1 - tanh( g + sqrt( Δ) * x)^2)
	end

	# get ε

	function get_ε( χ, g, ω)

		no_nodes = size( χ)[ 1]
		ε = Array{Float64}( no_nodes)

		for i in 1:no_nodes

			Δ = get_Δ( χ, ω, i)

			function inside_ε( x)

				return outside_ε( Δ, g[ i], x)
			end

			ε[ i] = - sqrt( 1 / 2 * π * Δ) * QuadGK.quadgk( inside_ε, -10.0, 10.0)[ 1]
		end

		return ε
	end

	# calculate the integral kernel to get ε

	function outside_ε( Δ, g, x)

		return exp( - x^2 / 2) * tanh( g + sqrt( Δ) * x) * ( 1 - tanh( g + sqrt( Δ) * x)^2) * x
	end

	# define kronecker δ

	function δ( i::Int64, j::Int64)

		if i == j

			return 1
		else

			return 0
		end
	end
end
