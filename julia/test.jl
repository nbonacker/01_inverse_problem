
# calculate λ_μ using the samples

function get_λ_μ_samples( samples, ω, u, μ)

	no_nodes = size( samples)[ 1]
	no_samples = size( samples)[ 2]
	λ_μ_samples = Array{Float64}( no_nodes)

	for i in 1:no_nodes

		foo = Array{Float64}( no_samples)

		for k in i:no_samples

			foo[ k] = 1 - tanh( transpose( ω[ i, :]) * samples[ :, k, μ] + u[ i, μ])^2
		end

		λ_μ_samples[ i] = mean( foo)
	end

	return λ_μ_samples
end

# calculate g_μ using the samples

function get_g_μ_samples( samples, ω, u, μ)

	no_nodes = size( samples)[ 1]
	no_samples = size( samples)[ 2]
	g_μ_samples = Array{Float64}( no_nodes)

	for i in 1:no_nodes

		foo = Array{Float64}( no_samples)

		for k in i:no_samples

			foo[ k] = transpose( ω[ i, :]) * samples[ :, k, μ] + u[ i, μ]
		end

		g_μ_samples[ i] = mean( foo)
	end

	return g_μ_samples
end

# test function gradient_gaussian_theory() with numeric differentiation

function test_gradient( ω, pert, data_mean, θ)

	no_nodes = size( ω)[ 1]
	test_∂f = zeros( no_nodes, no_nodes)

	Δ = 0.01
	x_0 = vec( ω)
	f_0 = generic_f( x_0, pert, data_mean, θ, [])

	for i in 1:no_nodes, j in 1:no_nodes

		if i == j continue end

		ω_1 = copy( ω)
		ω_1[ i, j] = ω_1[ i, j] + Δ
		x_1 = vec( ω_1)
		test_∂f[ i, j] = ( generic_f( x_1, pert, data_mean, θ, []) - f_0) / Δ
	end
	return vec( test_∂f)
end

# test ∂m ∂χ with numeric differentiation

function get_∂m_∂χ_difference_quotient_gaussian_theory( ω, pert, θ, μ, a, b, c)

	no_nodes = size( ω)[ 1]
	u = θ + pert[ :, μ]
	Δ = 0.01
	m_0, χ_0 = get_mχ_gaussian_theory( ω, u, a, b, c)
	∂m_difference_quotient = zeros( no_nodes, no_nodes, no_nodes)
	∂χ_difference_quotient = zeros( no_nodes, no_nodes, no_nodes, no_nodes)

	for i in 1:no_nodes, j in 1:no_nodes

		ω_1 = copy( ω)
		ω_1[ i, j] = ω_1[ i, j] + Δ
		m_1, χ_1 = get_mχ_gaussian_theory( ω_1, u, a, b, c)

		∂m_difference_quotient[ :, i, j] = ( m_1 - m_0)/Δ
		∂χ_difference_quotient[ :, :, i, j] = ( χ_1 - χ_0)/Δ
	end

	return ∂m_difference_quotient, ∂χ_difference_quotient
end

# test ∂m ∂χ with numeric differentiation

function get_∂m_∂χ_difference_quotient_exact_relations( ω, pert, θ, μ, samples)

	no_nodes = size( ω)[ 1]
	u = θ + pert[ :, μ]
	Δ = 0.01
	m_0, C_0 = get_mC_exact_relations( samples[ μ], ω, u)
	∂m_difference_quotient = zeros( no_nodes, no_nodes, no_nodes)
	∂χ_difference_quotient = zeros( no_nodes, no_nodes, no_nodes, no_nodes)

	for i in 1:no_nodes, j in 1:no_nodes

		ω_1 = copy( ω)
		ω_1[ i, j] = ω_1[ i, j] + Δ
		m_1, C_1 = get_mC_exact_relations( samples[ μ], ω_1, u)
		∂m_difference_quotient[ :, i, j] = ( m_1 - m_0)/Δ
		∂χ_difference_quotient[ :, :, i, j] = ( get_χ( C_1, m_1) - get_χ( C_0, m_0))/Δ
	end

	return ∂m_difference_quotient, ∂χ_difference_quotient
end

# test grad min squares with numeric differentiation

function get_grad_min_squares_difference_quotient( ω, pert, samples, m_samples, C_samples, θ, method)

	no_nodes = size( ω)[ 1]
	test_grad = zeros( no_nodes, no_nodes)

	Δ = 0.001
	x_0 = vec( ω)
	f_0 = generic_squares_second_order( x_0, pert, samples, m_samples, C_samples, θ, method, "flat", [])

	for i in 1:no_nodes, j in 1:no_nodes

		ω_1 = copy( ω)
		ω_1[ i, j] = ω_1[ i, j] + Δ
		x_1 = vec( ω_1)
		test_grad[ i, j] = ( generic_squares_second_order( x_1, pert, samples, m_samples, C_samples, θ, method, "flat", []) - f_0) / Δ
	end

	return vec( test_grad)
end

# test grad max likelihood with numeric differentiation

function get_grad_max_likelihood_difference_quotient( ω, pert, samples, θ, method)

	no_nodes = size( ω)[ 1]
	test_grad = zeros( no_nodes, no_nodes)

	Δ = 1e-4
	x_0 = vec( ω)
	f_0 = generic_log_likelihood( x_0, pert, samples, θ, method, "flat", [])

	for i in 1:no_nodes, j in 1:no_nodes

		ω_1 = copy( ω)
		ω_1[ i, j] = ω_1[ i, j] + Δ
		x_1 = vec( ω_1)
		test_grad[ i, j] = ( generic_log_likelihood( x_1, pert, samples, θ, method, "flat", []) - f_0) / Δ
	end

	return vec( test_grad)
end

# test hessian max likelihood with numeric differentiation

function get_hessian_max_likelihood_difference_quotient( ω_0, pert, samples, θ, method)

	no_nodes = size( ω)[ 1]
	hessian = zeros( no_nodes * no_nodes, no_nodes * no_nodes)

	Δ = 1e-4
	f_0 = generic_log_likelihood( vec( ω_0), pert, samples, θ, method, "flat", [])

	for i in 1:no_nodes, j in 1:no_nodes, k in 1:no_nodes, l in 1:no_nodes

		n = get_index( i, j, no_nodes)
		w = get_index( k, l, no_nodes)

		ω_1 = copy( ω_0)
		ω_1[ i, j] = ω_1[ i, j] + Δ
		ω_1[ k, l] = ω_1[ k, l] + Δ
		f_1 = generic_log_likelihood( vec( ω_1), pert, samples, θ, method, "flat", [])

		ω_2 = copy( ω_0)
		ω_2[ i, j] = ω_1[ i, j] + Δ
		f_2 = generic_log_likelihood( vec( ω_2), pert, samples, θ, method, "flat", [])

		ω_3 = copy( ω_0) 
		ω_3[ k, l] = ω_1[ k, l] + Δ
		f_3 = generic_log_likelihood( vec( ω_3), pert, samples, θ, method, "flat", [])

		hessian[ n, w] = ( f_1 - f_2 - f_3 + f_0) / ( Δ^2)
	end

	return hessian
end

# get index given i, j

function get_index( i, j, no_nodes)

	return Int( ( i - 1) * no_nodes + j)
end

# get index given k

function get_index( k, no_nodes)

	i = Int( floor( k / no_nodes) + 1)
	j = k % no_nodes
	return i, j
end
