
# get χ_μ given C_μ and m_μ

function get_χ( C, m)

	χ = C - m * transpose( m)

	return χ
end

# get C given χ and m

function get_C( χ, m)

	C = χ + m * transpose( m)

	return C
end

# get ∂χ_μ given ∂C_μ and ∂m_μ

function get_∂χ( ∂C, ∂m, m)

	no_nodes = size( m)[ 1]
	∂χ = Array{Float64}( no_nodes, no_nodes, no_nodes, no_nodes)

	for i in 1:no_nodes, j in 1:no_nodes

		∂χ[ i, j, :, :] = ∂C[ i, j, :, :] - m[ i] * ∂m[ j, :, :] - m[ j] * ∂m[ i, :, :]
	end

	return ∂χ
end

# get ∂C_μ given ∂χ_μ and ∂m_μ

function get_∂C( ∂χ, ∂m, m)

	no_nodes = size( m)[ 1]
	∂C = Array{Float64}( no_nodes, no_nodes, no_nodes, no_nodes)

	for i in 1:no_nodes, j in 1:no_nodes

		∂C[ i, j, :, :] = ∂χ[ i, j, :, :] + m[ i] * ∂m[ j, :, :] + m[ j] * ∂m[ i, :, :]
	end

	return ∂C
end

# return the symmetric M_s and the asymmetric M_a part of a given matrix M

function get_sym_asym( M)

	if size( M)[ 1] != size( M)[ 1]

		println( "no symmetric matrix")
		throw( DimensionMismatch)
	end

	no_nodes = size( M)[ 1]
	M_s = Array{Float64}( no_nodes, no_nodes)
	M_a = Array{Float64}( no_nodes, no_nodes)

	for i in 1:no_nodes, j in 1:no_nodes

		M_s[ i, j] = 0.5 * ( M[ i, j] + M[ j, i])
		M_a[ i, j] = 0.5 * ( M[ i, j] - M[ j, i])
	end

	return M_s, M_a
end

# calculate the correlation

function get_Ω( x)

	no_pertubations = size( x)[ 1]
	no_nodes = size( x[ 1])[1]
	no_samples = size( x[ 1])[ 2]
	three_point_correlation = [ Array{Float64}( no_nodes, no_nodes, no_nodes) for idx in 1:no_pertubations]

	for μ in 1:no_pertubations

		for i in 1:no_nodes, j in 1:i, k in 1:j

			a = Array{Float64}( no_samples)

			for t in 1:no_samples

				a[t] = x[ μ][ i, t]*x[ μ][ j, t] * x[ μ][ k, t]
			end

			three_point_correlation[ μ][ i, j, k] = three_point_correlation[ μ][ j, k, i] = three_point_correlation[ μ][ k, i, j] = three_point_correlation[ μ][ j, i, k] = three_point_correlation[ μ][ i, k, j] = three_point_correlation[ μ][ k, j, i] = mean(a)
		end
	end

	return three_point_correlation
end

# calculate the absolute deviation

function get_absolute_error( a, b)

	vec_a = vec(a)
	vec_b = vec(b)
	no_items = length( vec_a)
	foo = Array{Float64}( no_items)

	for i in 1:no_items

		foo[ i] = abs( (vec_a[ i] - vec_b[ i]))
	end

	return mean( foo), std( foo)
end

# calculate the squared deviation use like get_squared_deviation( ω, ω_infered)

function get_squared_error( a, b)

	no_nodes = size( a)[ 1]
	foo = 0.0
	bar = 0.0

	for i in 1:no_nodes, j in 1:no_nodes

		foo += (a[ i, j] - b[ i, j])^2
		bar += a[ i, j]^2
	end

	return sqrt( foo / bar)
end

# define kronecker δ

function δ( i::Int64, j::Int64)

	if i == j

		return 1
	else

		return 0
	end
end
