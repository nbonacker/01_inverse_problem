
module NetworkInference

	export max_likelihood, generic_log_likelihood, min_squares, generic_squares_first_order, generic_squares_second_order, get_squared_error, get_errors_averaged, get_samples_from_data_matrix, get_ω_bounds_sander_2013, rescale_data_sander_2013!, get_ω_filtered, get_ω_truncated, devide_data, get_prediction_error, concatenate

	using Distributions, SpecialFunctions, LinearAlgebra, NLopt, SharedArrays

	include( "simulation.jl")
	using .Simulation	# for the function get_m_samples, get_C_samples, get_C
	include( "gaussian_theory.jl")
	using .GaussianTheory
	include( "exact_relations.jl")
	using .ExactRelations

	struct GradNotImplemented <: Exception end

	# infere the coupling matrix ω given the data and the pertubation u

	function max_likelihood( pert, data, θ, back_end_method, ln_opt_method, prior, a, b, c, x_tolerance, max_evaluations, start_vector, search_space)

		no_nodes = size( data[ 1])[ 1]
		no_interactions = no_nodes^2
		opt = Opt( ln_opt_method, no_interactions)
		opt.xtol_abs = x_tolerance
		opt.maxeval = Int( max_evaluations)

		lb = search_space[ "lb"]
		ub = search_space[ "ub"]

		for i in 0 : ( no_nodes - 1)

			lb[ 1 + i * ( no_nodes + 1)] = 0
			ub[ 1 + i * ( no_nodes + 1)] = 0
		end

		opt.lower_bounds = lb
		opt.upper_bounds = ub
		runs = 0

		function local_log_likelihood( x, grad)

			runs += 1

			return generic_log_likelihood( x, pert, data, θ, back_end_method, prior, a, b, c, grad)
		end

		opt.max_objective = local_log_likelihood
		max_log_likelihood, x, ret = optimize( opt, start_vector)

		println( "max log likelihood : ", max_log_likelihood)
		println( "ret : ", ret)
		println( "runs : ", runs)

		return reshape( x, ( no_nodes, no_nodes))
	end

	# infere the coupling matrix ω and the model parameter a, b and c given the data and the pertubation u

	function max_likelihood( pert, data, back_end_method, ln_opt_method, prior, x_tolerance, max_evaluations, start_vector, search_space)

		no_nodes = size( data[ 1])[ 1]
		no_interactions = no_nodes^2
		opt = Opt( ln_opt_method, no_interactions + 4 * no_nodes)
		opt.xtol_abs = x_tolerance
		opt.maxeval = Int( max_evaluations)

		lb = search_space[ "lb"]
		ub = search_space[ "ub"]

		# set diagonal elements equal to zero

		for i in 0 : ( no_nodes - 1)

			lb[ 1 + i * ( no_nodes + 1)] = 0
			ub[ 1 + i * ( no_nodes + 1)] = 0
		end

		opt.lower_bounds = lb
		opt.upper_bounds = ub
		runs = 0

		function local_log_likelihood( y, grad)

			runs += 1
			x = y[ 1:no_interactions]
			a = y[ ( no_interactions + 1) : ( no_interactions + no_nodes)]
			b = y[ ( no_interactions + 1 * no_nodes + 1) : ( no_interactions + 2 * no_nodes)]
			c = y[ ( no_interactions + 2 * no_nodes + 1) : ( no_interactions + 3 * no_nodes)]
			θ = y[ ( no_interactions + 3 * no_nodes + 1) : ( no_interactions + 4 * no_nodes)]

			return generic_log_likelihood( x, pert, data, θ, back_end_method, prior, a, b, c, grad)
		end

		opt.max_objective = local_log_likelihood
		max_log_likelihood, y, ret = optimize( opt, start_vector)

		println( "max log likelihood : ", max_log_likelihood)
		println( "ret : ", ret)
		println( "runs : ", runs)

		ω = reshape( y[ 1:no_interactions], ( no_nodes, no_nodes))
		a = y[ ( no_interactions + 1) : ( no_interactions + no_nodes)]
		b = y[ ( no_interactions + 1 * no_nodes + 1) : ( no_interactions + 2 * no_nodes)]
		c = y[ ( no_interactions + 2 * no_nodes + 1) : ( no_interactions + 3 * no_nodes)]
		θ = y[ ( no_interactions + 3 * no_nodes + 1) : ( no_interactions + 4 * no_nodes)]

		return ω, a, b, c, θ
	end

	# calculate the log likelihood given the interaction vector x

	function generic_log_likelihood( x, pert, data, θ, back_end_method, prior, a, b, c, grad)

		no_pertubations = size( data)[ 1]
		no_nodes = size( data[ 1])[ 1]
		no_samples = size( data[ 1])[ 2]
		ω = reshape( x, ( no_nodes, no_nodes))
		log_likelihood = 0.0

		if length( grad) > 0

			∂log_likelihood = zeros( no_nodes, no_nodes)
		end

		for μ in 1:no_pertubations

			u = θ + pert[ :, μ]

			if back_end_method == "gaussian theory"

				m, χ = get_mχ_gaussian_theory( ω, u, a, b, c)
				χ_inverse = pinv( χ)

				if length( grad) > 0

					∂m, ∂χ = get_∂m_∂χ_gaussian_theory( ω, u, m, χ, a, b, c)
					∂C = get_∂C( ∂χ, ∂m, m)
				end

			elseif back_end_method == "linear theory"

				m, χ = get_mχ_linear_theory( ω, u, a, b, c)
				χ_inverse = pinv( χ)

				if length( grad) > 0

					println( "gradient is jet not implemented for the linear theory")
					throw( NoMethodType)
				end

			elseif back_end_method == "exact relations"

				m, C = get_mC_exact_relations( data[ μ], ω, u)
				χ = get_χ( C, m)
				χ_inverse = pinv( χ)

				if length( grad) > 0

					∂m, ∂C, ∂χ = get_∂m_∂C_∂χ_exact_relations( data[ μ], ω, u, m)
				end
			else

				println( "no mathing back end method")
				throw( NoMethodType)
			end

			for k in 1:no_samples

				log_likelihood -= 0.5 * transpose( data[ μ][ :, k] - m) * χ_inverse * ( data[ μ][ :, k] - m)
			end

			if length( grad) > 0

				for k in 1:no_samples

					for n in 1:no_nodes, w in 1:no_nodes

						∂log_likelihood[ n, w] += 0.5 * transpose( ∂m[ :, n, w]) * χ_inverse * ( data[ μ][ :, k] - m) + 0.5 * transpose( data[ μ][ :, k] - m) * χ_inverse * ( ∂m[ :, n, w])
						∂log_likelihood[ n, w] += 0.5 * transpose( data[ μ][ :, k] - m) * χ_inverse * ∂χ[ :, :, n, w] * χ_inverse * ( data[ μ][ :, k] - m)
					end
				end

				for i in 1:no_nodes, j in 1:no_nodes

					∂log_likelihood -= 0.5 * no_samples * χ_inverse[ j, i] .* ∂χ[ i, j, :, :]
				end
			end

			foo = det( χ)

			if foo > 0

				log_likelihood -= 0.5 * no_samples * log( foo)
			end
		end

		if prior[ 1] == "gaussian" # prior = ( "gaussian", μ, σ) with mean μ and standard deviation σ

			μ = prior[ 2]
			σ = prior[ 3]

			for i in 1:no_nodes, j in 1:no_nodes

				log_likelihood -= 0.5 * ( ( ω[ i, j] - μ) / σ)^2
			end

			if length( grad) > 0

				throw( GradNotImplemented())
			end
		elseif prior[ 1] == "laplace" # prior = ( "laplace", μ, b) with mean μ and diversity b

			μ = prior[ 2]
			b = prior[ 3]

			for i in 1:no_nodes, j in 1:no_nodes

				log_likelihood -= abs( ω[ i, j] - μ) / b
			end

			if length( grad) > 0

				throw( GradNotImplemented())
			end
		end

		if length( grad) > 0

			∂log_likelihood = vec( ∂log_likelihood)

			for i in 1:length( ∂log_likelihood)

				grad[ i] = ∂log_likelihood[ i]
			end
		end

		# println( "generic_log_likelihood")

		return log_likelihood
	end

	# infere the coupling matrix ω given the data and the pertubation u

	function min_squares( pert, data, θ, back_end_method, ln_opt_method, prior, a, b, c, x_tolerance, max_evaluations, start_vector, search_space)

		no_nodes = size( data[ 1])[ 1]
		no_interactions = no_nodes^2
		opt = Opt( ln_opt_method, no_interactions)
		opt.xtol_abs = x_tolerance
		opt.maxeval = Int( max_evaluations)

		lb = search_space[ "lb"]
		ub = search_space[ "ub"]

		for i in 0 : ( no_nodes - 1)

			lb[ 1 + i * ( no_nodes + 1)] = 0
			ub[ 1 + i * ( no_nodes + 1)] = 0
		end

		opt.lower_bounds = lb
		opt.upper_bounds = ub

		m_data = get_m_simulation( data)
		C_data = get_C_simulation( data)

		runs = 0

		function local_squares_first_order( x, grad)

			runs += 1
			return generic_squares_first_order( x, pert, data, m_data, θ, back_end_method, prior, a, b, c, grad)
		end

		function local_squares_second_order( x, grad)

			runs += 1
			return generic_squares_second_order( x, pert, data, m_data, C_data, θ, back_end_method, prior, a, b, c, grad)
		end

		if back_end_method == "exact relations first order" || back_end_method == "naive theory"

			# println( "use local squares first order!")
			min_objective!( opt, local_squares_first_order)
		else

			# println( "use local squares second order!")
			min_objective!( opt, local_squares_second_order)
		end

		min_squared_deviation, x, ret = optimize( opt, start_vector)

		println( "min squared deviation : ", min_squared_deviation)
		println( "ret : ", ret)
		println( "runs : ", runs)

		return reshape( x, ( no_nodes, no_nodes))
	end

	# infere the coupling matrix ω and model parameter a, b, c given the data and the pertubation u

	function min_squares( pert, data, back_end_method, ln_opt_method, prior, x_tolerance, max_evaluations, start_vector, search_space)

		no_nodes = size( data[ 1])[ 1]
		no_interactions = no_nodes^2
		opt = Opt( ln_opt_method, no_interactions + 4 * no_nodes)
		opt.xtol_abs = x_tolerance
		opt.maxeval = Int( max_evaluations)

		lb = search_space[ "lb"]
		ub = search_space[ "ub"]

		for i in 0 : ( no_nodes - 1)

			lb[ 1 + i * ( no_nodes + 1)] = 0
			ub[ 1 + i * ( no_nodes + 1)] = 0
		end

		opt.lower_bounds = lb
		opt.upper_bounds = ub

		m_data = get_m_simulation( data)
		C_data = get_C_simulation( data)

		runs = 0

		function local_squares_first_order( y, grad)

			runs += 1
			x = y[ 1:no_interactions]
			a = y[ ( no_interactions + 1) : ( no_interactions + no_nodes)]
			b = y[ ( no_interactions + 1 * no_nodes + 1) : ( no_interactions + 2 * no_nodes)]
			c = y[ ( no_interactions + 2 * no_nodes + 1) : ( no_interactions + 3 * no_nodes)]
			θ = y[ ( no_interactions + 3 * no_nodes + 1) : ( no_interactions + 4 * no_nodes)]
			return generic_squares_first_order( x, pert, data, m_data, θ, back_end_method, prior, a, b, c, grad)
		end

		function local_squares_second_order( y, grad)

			runs += 1
			x = y[ 1:no_interactions]
			a = y[ ( no_interactions + 1) : ( no_interactions + no_nodes)]
			b = y[ ( no_interactions + 1 * no_nodes + 1) : ( no_interactions + 2 * no_nodes)]
			c = y[ ( no_interactions + 2 * no_nodes + 1) : ( no_interactions + 3 * no_nodes)]
			θ = y[ ( no_interactions + 3 * no_nodes + 1) : ( no_interactions + 4 * no_nodes)]

			return generic_squares_second_order( x, pert, data, m_data, C_data, θ, back_end_method, prior, a, b, c, grad)
		end

		if back_end_method == "exact relations first order" || back_end_method == "naive theory"

			# println( "use local squares first order!")
			min_objective!( opt, local_squares_first_order)
		else

			# println( "use local squares second order!")
			min_objective!( opt, local_squares_second_order)
		end

		min_squared_deviation, y, ret = optimize( opt, start_vector)

		println( "min squared deviation : ", min_squared_deviation)
		println( "ret : ", ret)
		println( "runs : ", runs)

		ω = reshape( y[ 1:no_interactions], ( no_nodes, no_nodes))
		a = y[ ( no_interactions + 1) : ( no_interactions + no_nodes)]
		b = y[ ( no_interactions + 1 * no_nodes + 1) : ( no_interactions + 2 * no_nodes)]
		c = y[ ( no_interactions + 2 * no_nodes + 1) : ( no_interactions + 3 * no_nodes)]
		θ = y[ ( no_interactions + 3 * no_nodes + 1) : ( no_interactions + 4 * no_nodes)]

		return ω, a, b, c, θ
	end

	# calculate the squared deviation given the interaction vector x in first order

	function generic_squares_first_order( x, pert, data, m_data, θ, back_end_method, prior, a, b, c, grad)

		no_pertubations = size( data)[ 1]
		no_nodes = size( data[ 1])[ 1]
		no_samples = size( data[ 1])[ 2]
		ω = reshape( x, ( no_nodes, no_nodes))
		squared_deviation = 0.0

		if length( grad) > 0

			∂squared_deviation = zeros( no_nodes, no_nodes)
		end

		for μ in 1:no_pertubations

			u = θ + pert[ :, μ]

			if back_end_method == "gaussian theory"

				m, C = get_mC_gaussian_theory( ω, u, a, b, c)

				if length( grad) > 0

					χ = get_χ( C, m)
					∂m, ∂χ = get_∂m_∂χ_gaussian_theory( ω, u, m, χ, a, b, c)
					∂C = get_∂C( ∂χ, ∂m, m)
				end

			elseif back_end_method == "naive theory"

				m, C = get_mC_naive_theory( m_data[ μ], ω, u, a, b, c)

				if length( grad) > 0

							println( "function grad is not implemented for naive theory")
							throw( NoMethodType)
						end

			elseif back_end_method == "exact relations first order"

				m = get_m_exact_relations( data[ μ], ω, u, a, b)

				if length( grad) > 0

					∂m, ∂C, ∂χ = get_∂m_∂C_∂χ_exact_relations( data[ μ], ω, u, m, a, b, c)
				end
			end

			for i in 1:no_nodes

				squared_deviation += ( m_data[ μ][ i] - m[ i])^2
			end

			if length( grad) > 0

				for i in 1:no_nodes

					∂squared_deviation -= 2 * ( m_data[ μ][ i] - m[ i]) * ∂m[ i , :, :]
				end
			end
		end

		if prior[ 1] == "gaussian" # prior = ( "gaussian", μ, σ) with mean μ and standard deviation σ

			μ = prior[ 2]
			σ = prior[ 3]

			for i in 1:no_nodes, j in 1:no_nodes

				log_likelihood -= 0.5 * ( ( ω[ i, j] - μ) / σ)^2
			end

			if length( grad) > 0

				throw( GradNotImplemented())
			end
		elseif prior[ 1] == "laplace" # prior = ( "laplace", μ, b) with mean μ and diversity b

			μ = prior[ 2]
			b = prior[ 3]

			for i in 1:no_nodes, j in 1:no_nodes

				log_likelihood -= abs( ω[ i, j] - μ) / b
			end

			if length( grad) > 0

				throw( GradNotImplemented())
			end
		end

		if length( grad) > 0

			∂squared_deviation = vec( ∂squared_deviation)

			for i in 1:length( ∂squared_deviation)

				grad[ i] = ∂squared_deviation[ i]
			end
		end

		return squared_deviation
	end

	# calculate the squared deviation given the interaction vector x in second	 order

	function generic_squares_second_order( x, pert, data, m_data, C_data, θ, back_end_method, prior, a, b, c, grad)

		no_pertubations = size( data)[ 1]
		no_nodes = size( data[ 1])[ 1]
		no_samples = size( data[ 1])[ 2]
		ω = reshape( x, ( no_nodes, no_nodes))
		squared_deviation = 0.0
		m = zeros( no_nodes)
		χ = zeros( no_nodes, no_nodes)
		C = zeros( no_nodes, no_nodes)

		if length( grad) > 0

			∂squared_deviation = zeros( no_nodes, no_nodes)
		end

		for μ in 1:no_pertubations

			u = θ + pert[ :, μ]

			if back_end_method == "gaussian theory"

				m, χ = get_mχ_gaussian_theory( ω, u, a, b, c)
				C = get_C(	χ, m)

				if length( grad) > 0

					∂m, ∂χ = get_∂m_∂χ_gaussian_theory( ω, u, m, χ, a, b, c)
					∂C = get_∂C( ∂χ, ∂m, m)
				end
			elseif back_end_method == "exact relations second order"

				m, C = get_mC_exact_relations( data[ μ], ω, u, a, b, c)

				if length( grad) > 0

					∂m, ∂C, ∂χ = get_∂m_∂C_∂χ_exact_relations( data[ μ], ω, u, m, a, b,	 c)
				end
			end

			for i in 1:no_nodes

				squared_deviation += ( m_data[ μ][ i] - m[ i])^2

				for j in 1:i

					squared_deviation += 1 / no_nodes * ( C_data[ μ][ i, j] - C[ i, j])^2
				end
			end

			if length( grad) > 0

				for i in 1:no_nodes

					∂squared_deviation -= 2 * ( m_data[ μ][ i] - m[ i]) * ∂m[ i , :, :]

					for j in 1:i

						∂squared_deviation -= 2 * ( C_data[ μ][ i, j] - C[ i, j]) * ∂C[ i, j, :, :]
					end
				end
			end
		end

		if prior[ 1] == "gaussian" # prior = ( "gaussian", μ, σ) with mean μ and standard deviation σ

			μ = prior[ 2]
			σ = prior[ 3]

			for i in 1:no_nodes, j in 1:no_nodes

				log_likelihood -= 0.5 * ( ( ω[ i, j] - μ) / σ)^2
			end

			if length( grad) > 0

				throw( GradNotImplemented())
			end
		elseif prior[ 1] == "laplace" # prior = ( "laplace", μ, b) with mean μ and diversity b

			μ = prior[ 2]
			b = prior[ 3]

			for i in 1:no_nodes, j in 1:no_nodes

				log_likelihood -= abs( ω[ i, j] - μ) / b
			end

			if length( grad) > 0

				throw( GradNotImplemented())
			end
		end


		if length( grad) > 0

			∂squared_deviation = vec( ∂squared_deviation)

			for i in 1:length( ∂squared_deviation)

				grad[ i] = ∂squared_deviation[ i]
			end
		end

		# println( "grad : ", grad)

		return squared_deviation
	end

	# calculate the squared deviation use like get_squared_deviation( ω, ω_infered)

	function get_squared_error( a, b)

		no_nodes = size( a)[ 1]
		foo = 0.0
		bar = 0.0

		for i in 1:no_nodes, j in 1:no_nodes

			foo += (a[ i, j] - b[ i, j])^2
			bar += a[ i, j]^2
		end

		return sqrt( foo / bar)
	end

	# get errors averaged

	function get_errors_averaged( errors)

		no_methods = size( errors)[ 1]
		length = size( errors)[ 2]
		no_runs = size( errors)[ 3]

		errors_averaged = Array{Float64}( undef, no_methods, length)
		std_deviation = Array{Float64}( undef, no_methods, length)

		for m in 1:no_methods, l in 1:length

			errors_averaged[ m, l] = mean( errors[ m, l, :])
			std_deviation[ m, l] = std( errors[ m, l, :])
		end

		return errors_averaged, std_deviation
	end

	# get samples given data matrix

	function get_samples_from_data_matrix( data_matrix)

		no_nodes, no_pert = size( data_matrix)
		no_samples_per_pert = 1

		samples = [ Array{Float64}( undef, no_nodes, no_samples_per_pert) for idx in 1:no_pert]

		for μ in 1:no_pert

			for i in 1:no_nodes

				samples[ μ][ i] = data_matrix[ i, μ]
			end
		end

		return samples
	end

	# get search_space_sanders_2013 with no self interactions and measured and perturbed nodes
	# search_space_sanders_2013 = get_search_space_sanders_2013()

	function get_ω_bounds_sander_2013()

		no_nodes = 25
		no_measured_nodes = 17
		no_perturbed_nodes = 8

		ω = Array{Bool}( undef, no_nodes, no_nodes)

		for i in 1:no_nodes, j in 1:no_nodes

			if i == j

				ω[ i, j] = false
			elseif i > no_measured_nodes

				ω[ i, j] = false
			else

			ω[ i, j] = true
			end
		end

		ω_vector = reshape( ω, ( no_nodes^2, 1))

		ω_lb = Array{Float64}( undef, no_nodes^2)
		ω_ub = Array{Float64}( undef, no_nodes^2)

		for k in 1:no_nodes^2

			if ω_vector[ k]

				ω_lb[ k] = -1.0
				ω_ub[ k] = +1.0
			else

				ω_lb[ k] = -0.0
				ω_ub[ k] = +0.0
			end
		end

		return ω_lb, ω_ub
	end

	# rescale data for dataset sander2013

	function rescale_data_sander_2013!( data)

		no_nodes = 25
		no_measured_nodes = 17

		foo = maximum( abs.( data[ 1:no_measured_nodes, :]))

		for i in 1:no_measured_nodes

			data[ i, :] = data[ i, :] ./ foo
		end
	end

	# filter infered ω using threshold δ

	function get_ω_filtered( ω, δ)

		no_nodes = size( ω)[ 1]
		ω_filtered = Array{Float64}( undef, no_nodes, no_nodes)

		for i in 1:no_nodes, j in 1:no_nodes

				if abs( ω[ i, j]) >= δ

					ω_filtered[ i, j] = ω[ i, j]
				else

			ω_filtered[ i, j] = 0
			end
		end

		return ω_filtered
	end

	# truncate element k in infered ω

	function get_ω_truncated( ω, k)

		no_nodes = size( ω)[ 1]
		ω_truncated = ω[ 1:no_nodes .!= k, 1:no_nodes .!= k]

		return ω_truncated
	end

	# divide data into trainigs set and prediction set

	function devide_data( data, list_predictions)

		no_nodes = size( data)[ 1]
		no_perturbations = size( data)[ 2]
		no_predictions = length( list_predictions)
		list_perturbations = Array( 1:no_perturbations)
		list_trainings = filter( foo -> !( foo in list_predictions), list_perturbations)
		data_training = data[ :, list_trainings]
		data_prediction = data[ :, list_predictions]

		return data_training, data_prediction
	end

	# get mean and std of prediction error

	function get_prediction_error( x, y)

		foo = vec( abs.( x .- y))
		mean_diff = mean( foo)
		std_diff = std( foo)

		return mean_diff, std_diff
	end

	# concatenate to one vector

	function concatenate( v)

		no_elements = length( v)
		v_cat = Array{Float64}( undef, 0)

		for i in 1:no_elements

			append!( v_cat, v[ i])
		end

		return v_cat
	end
end
