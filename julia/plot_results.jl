
module PlotResults

	export scatter_forward_problem, plot_error, plot_time_evolution, histogramm_error

	using Plots, StatsPlots, Distributions, Plots.PlotMeasures, ColorSchemes, LaTeXStrings

	default( legend = false, legendfont = ( 7, "times"), guidefont = ( 7, "times"), tickfont = ( 7, "times"), framestyle = :box)

	# make a scatter plot betwen two vectors

	function scatter_forward_problem( x_data, y_data, x_label, y_label, x_lim, y_lim, x_ticks, y_ticks, color_list, output_pdf)

		no_data = length( y_data)
		plot( size = ( 200, 200))

		for i in 1:no_data

			scatter!( vec( x_data), vec( y_data[ i]), alpha = 1.0, color = color_list[ i], markersize = 4)
		end

		plot!( xlabel = LaTeXString( x_label), ylabel = LaTeXString( y_label), xlim = x_lim, ylim = y_lim, aspect_ratio = :equal)
		plot!( yrotation = 60, grid = true, xticks = x_ticks, yticks = y_ticks, tick_direction = :out)
		savefig( output_pdf)
	end

	# plot errors

	function plot_error( x_axis, error_mean, errors_std, colors, x_label, y_label, x_lim, y_lim, xscale, yscale, output_pdf)

		no_methods = size( error_mean)[ 1]

		plot( size = ( 200, 200))

		for i in 1:no_methods

			# plot!( x_axis, error_mean[ i, :], yerr = errors_std[ i, :], color = colors[ i], linewidth = 2, alpha = 1.0)
			plot!( x_axis, error_mean[ i, :], yerr = errors_std[ i, :], ribbon = errors_std[ i, :], fillalpha = 0.2, color = colors[ i], linewidth = 2, alpha = 1.0)
		end

		# plot!( yrotation = 60, grid = true, xlim = x_lim, ylim = y_lim, aspect_ratio = :equal, tick_direction = :out, xlabel = LaTeXString( x_label), ylabel = LaTeXString( y_label))
		# plot!( yrotation = 60, grid = true, xlim = x_lim, ylim = y_lim, tick_direction = :out, xlabel = LaTeXString( x_label), ylabel = LaTeXString( y_label))
		r = ( x_lim[ 2] - x_lim[ 1]) / ( y_lim[ 2] - y_lim[ 1])
		plot!( yrotation = 60, grid = true, xlim = x_lim, ylim = y_lim, aspect_ratio = r, tick_direction = :out, xlabel = LaTeXString( x_label), ylabel = LaTeXString( y_label))

		if yscale == "log"

			plot!( yscale = :log)
		end

		if xscale == "log"

			plot!( xscale = :log)
		end

		# tick_params!( "both", labelsize = 10, direction = "out")

		# ax[:legend]( fontsize = 10, loc = "lower right", fancybox = true, shadow = true , bbox_to_anchor = (0.0, 0.0))

		# ax[:spines]["top"][:set_color]("none") # Remove the top axis boundary
		# ax[:spines]["right"][:set_color]("none") # Remove the right axis boundary


		# PyPlot.axis( "equal")
		savefig( output_pdf)
	end

	# plot time evolution

	function plot_time_evolution( time_evolution, output_pdf)

		no_nodes = size( time_evolution)[ 1]
		no_samples = size( time_evolution)[ 2]

		plot( size = ( 200, 200))

		for i in 1:no_nodes

			x = time_evolution[ i, :]
			c = get( ColorSchemes.rainbow, i / no_nodes)
			plot!( x, linewidth = 2, color = c)
		end

		plot!( yaxis = ( ( -1.2, 1.2), -1.2:0.4:1.2), yrotation = 60, grid = true)
		plot!( tick_direction = :out, xlabel = LaTeXString( "time"), ylabel = LaTeXString( "gene expression"))

		savefig( output_pdf)
	end

	# histogramm prediction error

	function histogramm_error( error, color, distribution, output_pdf)

    no_errors = length( error)
		max_error = 0.04
		plot( size = ( 150, 150))
		histogram!( error, bins = Array( 0 : max_error / 12 : max_error), fillcolor = color, normalize = true, fillrange = 0, xlim = ( 0, max_error), ylim = ( 1, 1000), yscale = :log10)
		plot!( distribution, linewidth = 2.0, linestyle = :dash, color = :black)

		plot!( xrotation = 60, yrotation = 60, grid = true, size = ( 150, 150), xlabel = "squared error")
    savefig( output_pdf)
  end
end
