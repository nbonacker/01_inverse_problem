
# calculate the mean m and the correlation C given interaction ω and pertubation u

function get_mC_naive_approach( samples, ω, u, a, b, c)

	# get the mean expression level samples_mean

	samples_mean = get_m_samples( samples)

	m = get_m_naive_approach( samples_mean, ω, u, a, b)
	C = get_C_naive_approach( samples_mean, ω, u, a, b, c)

	return m, C
end

# calculate mean vector m with naive approach

function get_m_naive_approach( samples_mean, ω, u, a, b)

	no_nodes = size( samples)[1]
	m = Array{Float64}( no_nodes)

	for i in 1:no_nodes

		m[ i] = a[ i] / b[ i] * tanh( transpose( ω[ i, :]) * samples_mean + u[ i])
	end

	return m
end

# calculate the correlation matrix C with naive

function get_C_naive_approach( samples_mean, ω, u, a, b, c)

	no_nodes = size( samples)[1]
	no_samples = size( samples)[ 2]

	C = Array{Float64}( no_nodes, no_nodes)

	for i in 1:no_nodes, j in 1:i

		C[ i, j] = C[ j, i] = a[ i] / ( b[ i] + b[ j]) * tanh( transpose( ω[ i, :]) * samples_mean + u[ i]) * samples_mean[ j] + a[ j] / ( b[ i] + b[ j]) * tanh( transpose( ω[ j, :]) * samples_mean + u[ j]) * samples_mean[ i] + 0.5 * c[ i]^2 * δ( i, j)
	end

	return C
end
