
# start or attach tmux session

tmux new -s 'sander_2013_with_parameter_optimization'
tmux attach-session -t 'sander_2013_with_parameter_optimization'



# set up julia for mac12

/Applications/Julia-1.2.app/Contents/Resources/julia/bin/julia
using Distributed
no_procs = 4
addprocs( no_procs)
@everywhere project_folder = "/Users/nbonacker/Projects/01_inverse_problem/"
@everywhere cd( project_folder)



# set up julia for Niklas-ThinkPad-L13-Yoga

/home/niklas/Source/julia-1.4.1/bin/julia
using Distributed
no_procs = 2
addprocs( no_procs)
@everywhere project_folder = "/home/niklas/Projects/01_inverse_problem/"
@everywhere cd( project_folder)



# define name and make dir

name = "sander_2013"
data_folder = project_folder * "data/Sander2013/DataSet-S1/"
output_folder = project_folder * "output/sander_2013_with_parameter_optimization/"
plots_folder = project_folder * "plots/sander_2013_with_parameter_optimization/"

output_ms1o = output_folder * "results_ms1o"
output_ms2o = output_folder * "results_ms2o"
output_msGt = output_folder * "results_msGt"
output_mlGt = output_folder * "results_mlGt"
mkpath( output_folder)
mkpath( plots_folder)



# use packages

@everywhere using Revise, SharedArrays, JLD2, DelimitedFiles, Statistics, ColorSchemes, Distributions, StatsBase
@everywhere includet( project_folder * "julia/simulation.jl")
@everywhere using .Simulation
@everywhere includet( project_folder * "julia/network_inference.jl")
@everywhere using .NetworkInference
@everywhere includet( project_folder * "julia/plot_results.jl")
@everywhere using .PlotResults
@everywhere includet( project_folder * "julia/gaussian_theory.jl")
@everywhere using .GaussianTheory



# load and rescale data

data = readdlm( data_folder * "data.txt")
rescale_data_sander_2013!( data)
input = readdlm( data_folder * "input.txt")
name = readdlm( data_folder * "name.txt")
pert = readdlm( data_folder * "pert.txt")
no_nodes = size( data)[ 1]



# devide data into trainig sets

list_predictions = [ [ 1, 11, 21, 31, 41], [ 2, 12, 22, 32, 42], [ 3, 13, 23, 33, 43], [ 4, 14, 24, 34, 44]]
list_data_training = Array{Array{Float64,2}}( undef, 4)
list_data_prediction = Array{Array{Float64,2}}( undef, 4)
list_pert_training = Array{Array{Float64,2}}( undef, 4)
list_pert_prediction = Array{Array{Float64,2}}( undef, 4)
list_samples = Array{Array{Array{Float64}}}( undef, 4)

for t in 1:4

	data_training, data_prediction = devide_data( data, list_predictions[ t])
	pert_training, pert_prediction = devide_data( pert, list_predictions[ t])
	list_data_training[ t] = data_training
	list_data_prediction[ t] = data_prediction
	list_pert_training[ t] = data_training
	list_pert_prediction[ t] = data_prediction

	no_perturbations = size( data_training)[ 2]
	no_samples_per_pertubation = 1
	samples = [ Array{Float64}( undef, no_nodes, no_samples_per_pertubation) for i in 1:no_perturbations]

	for μ in 1:no_perturbations

		samples[ μ][ :, 1] = data_training[ :, μ]
	end

	list_samples[ t] = samples
end



# infere interaction matrix and model parameter based on training sets

ln_opt_method = :LN_BOBYQA
x_tolerance = 1e-2
max_evaluations = 1e+6

@sync @distributed for t in [ 1, 2, 3, 4] # t = 1

	no_nodes = size( data)[ 1]
	pert = list_pert_training[ t]
	samples = list_samples[ t]

	for c in [ 0.01, 0.02, 0.05, 0.1, 0.2, 0.5] # c = 0.1

		ω_lb, ω_ub = get_ω_bounds_sander_2013()
		a_lb, a_ub = ones( no_nodes), ones( no_nodes)
		b_lb, b_ub = zeros( no_nodes), ones( no_nodes)
		c_lb, c_ub = c * ones( no_nodes), c * ones( no_nodes)
		θ_lb, θ_ub = -0.5 * ones( no_nodes), 0.5 * ones( no_nodes)
		search_space = Dict( "lb" => concatenate( [ ω_lb, a_lb, b_lb, c_lb, θ_lb]), "ub" => concatenate( [ ω_ub, a_ub, b_ub, c_ub, θ_ub]))
		ω_start = zeros( no_nodes^2)
		a_start = ones( no_nodes)
		b_start = 0.5 * ones( no_nodes)
		c_start = c * ones( no_nodes)
		θ_start = zeros( no_nodes)
		y_start = concatenate( [ ω_start, a_start, b_start, c_start, θ_start])

		println( "start work on: t=", t, " c=", c)
		prior = "flat prior"
		no_samples_per_pertubation = 1
		no_nodes = size( pert)[ 1]
		no_perturbations = size( pert)[ 2]

		# use minumum square first order

		ω_ms1o, a_ms1o, b_ms1o, c_ms1o, θ_ms1o = min_squares( pert, samples, "exact relations first order", ln_opt_method, prior, x_tolerance, max_evaluations, y_start, search_space)
		results_ms1o = Dict( "ω_ms1o" => ω_ms1o, "a_ms1o" => a_ms1o, "b_ms1o" => b_ms1o, "c_ms1o" => c_ms1o, "θ_ms1o" => θ_ms1o)
		@save output_ms1o * "_c=" * string(c) * "_t=" * string(t)  * ".jld2" results_ms1o

		# use minumum square second order

		ω_ms2o, a_ms2o, b_ms2o, c_ms2o, θ_ms2o = min_squares( pert, samples, "exact relations second order", ln_opt_method, prior, x_tolerance, max_evaluations, y_start, search_space)
		results_ms2o = Dict( "ω_ms2o" => ω_ms2o, "a_ms2o" => a_ms2o, "b_ms2o" => b_ms2o, "c_ms2o" => c_ms2o, "θ_ms2o" => θ_ms2o)
		@save output_ms2o * "_c=" * string(c) * "_t=" * string(t)  * ".jld2" results_ms2o

		# use minumum square Gaussian theory

		ω_msGt, a_msGt, b_msGt, c_msGt, θ_msGt = min_squares( pert, samples, "gaussian theory", ln_opt_method, prior, x_tolerance, max_evaluations, y_start, search_space)
		results_msGt = Dict( "ω_msGt" => ω_msGt, "a_msGt" => a_msGt, "b_msGt" => b_msGt, "c_msGt" => c_msGt, "θ_msGt" => θ_msGt)
		@save output_msGt * "_c=" * string(c) * "_t=" * string(t)  * ".jld2" results_msGt

		# use maximum likelihood method

		ω_mlGt, a_mlGt, b_mlGt, c_mlGt, θ_mlGt = max_likelihood( pert, samples, "gaussian theory", ln_opt_method, prior, x_tolerance, max_evaluations, y_start, search_space)
		results_mlGt = Dict( "ω_mlGt" => ω_mlGt, "a_mlGt" => a_mlGt, "b_mlGt" => b_mlGt, "c_mlGt" => c_mlGt, "θ_mlGt" => θ_mlGt)
		@save output_mlGt * "_c=" * string(c) * "_t=" * string(t) * ".jld2" results_mlGt
		end
	end
end



# predict response and scatter plot results

x_label = "measured gene expression"
y_label = "predicted gene expression"
x_lim = [ -1, 1]
y_lim = [ -1, 1]
x_ticks = [ -1.0, -0.5, 0.0, 0.5, 1.0]
y_ticks = [ -1.0, -0.5, 0.0, 0.5, 1.0]
color_scheme = ColorSchemes.rainbow
color_list_1 = [ color_scheme[ 1/4], color_scheme[ 2/4]]
color_list_2 = [ color_scheme[ 3/4], color_scheme[ 4/4]]

@sync @distributed for t in [ 1, 2, 3, 4] # t = 1

	pert_prediction = list_pert_prediction[ t]
	data_prediction = list_data_prediction[ t]
	no_nodes = size( pert_prediction)[ 1]
	no_pert_prediction = size( pert_prediction)[ 2]

	for c in [ 0.01, 0.02, 0.05, 0.1, 0.2, 0.5] # c = 0.01

		println( "start work on: t=", t, " c=", c)
		u_ms1o = Array{ Float64, 2}( undef, no_nodes, no_pert_prediction)
		u_ms2o = Array{ Float64, 2}( undef, no_nodes, no_pert_prediction)
		u_msGt = Array{ Float64, 2}( undef, no_nodes, no_pert_prediction)
		u_mlGt = Array{ Float64, 2}( undef, no_nodes, no_pert_prediction)
		m_ms1o = Array{ Float64, 2}( undef, no_nodes, no_pert_prediction)
		m_ms2o = Array{ Float64, 2}( undef, no_nodes, no_pert_prediction)
		m_msGt = Array{ Float64, 2}( undef, no_nodes, no_pert_prediction)
		m_mlGt = Array{ Float64, 2}( undef, no_nodes, no_pert_prediction)
		@load output_ms1o * "_c=" * string(c) * "_t=" * string(t) * ".jld2" results_ms1o
		@load output_ms2o * "_c=" * string(c) * "_t=" * string(t) * ".jld2" results_ms2o
		@load output_msGt * "_c=" * string(c) * "_t=" * string(t) * ".jld2" results_msGt
		@load output_mlGt * "_c=" * string(c) * "_t=" * string(t) * ".jld2" results_mlGt
		ω_ms1o = results_ms1o[ "ω_ms1o"]
		a_ms1o = results_ms1o[ "a_ms1o"]
		b_ms1o = results_ms1o[ "b_ms1o"]
		c_ms1o = results_ms1o[ "c_ms1o"]
		θ_ms1o = results_ms1o[ "θ_ms1o"]
		ω_ms2o = results_ms2o[ "ω_ms2o"]
		a_ms2o = results_ms2o[ "a_ms2o"]
		b_ms2o = results_ms2o[ "b_ms2o"]
		c_ms2o = results_ms2o[ "c_ms2o"]
		θ_ms2o = results_ms2o[ "θ_ms2o"]
		ω_msGt = results_msGt[ "ω_msGt"]
		a_msGt = results_msGt[ "a_msGt"]
		b_msGt = results_msGt[ "b_msGt"]
		c_msGt = results_msGt[ "c_msGt"]
		θ_msGt = results_msGt[ "θ_msGt"]
		ω_mlGt = results_mlGt[ "ω_mlGt"]
		a_mlGt = results_mlGt[ "a_mlGt"]
		b_mlGt = results_mlGt[ "b_mlGt"]
		c_mlGt = results_mlGt[ "c_mlGt"]
		θ_mlGt = results_mlGt[ "θ_mlGt"]

		for μ in 1:no_pert_prediction

			u_ms1o[ :, μ] = pert_prediction[ :, μ] + θ_ms1o
			u_ms2o[ :, μ] = pert_prediction[ :, μ] + θ_ms2o
			u_msGt[ :, μ] = pert_prediction[ :, μ] + θ_msGt
			u_mlGt[ :, μ] = pert_prediction[ :, μ] + θ_mlGt
			m_ms1o[ :, μ], foo = get_mχ_gaussian_theory( ω_ms1o, u_ms1o[ :, μ], a_ms1o, b_ms1o, c_ms1o)
			m_ms2o[ :, μ], foo = get_mχ_gaussian_theory( ω_ms2o, u_ms2o[ :, μ], a_ms2o, b_ms2o, c_ms2o)
			m_msGt[ :, μ], foo = get_mχ_gaussian_theory( ω_msGt, u_msGt[ :, μ], a_msGt, b_msGt, c_msGt)
			m_mlGt[ :, μ], bar = get_mχ_gaussian_theory( ω_mlGt, u_mlGt[ :, μ], a_mlGt, b_mlGt, c_mlGt)
		end

		output_pdf_1 = plots_folder * "scatterplot_c=" * string(c) * "_t=" * string(t) * "_1.pdf"
		output_pdf_2 = plots_folder * "scatterplot_c=" * string(c) * "_t=" * string(t) * "_2.pdf"
		scatter_forward_problem( data_prediction, [ m_ms1o, m_ms2o], x_label, y_label, x_lim, y_lim, x_ticks, y_ticks, color_list_1, output_pdf)
		scatter_forward_problem( data_prediction, [ m_msGt, m_mlGt], x_label, y_label, x_lim, y_lim, x_ticks, y_ticks, color_list_2, output_pdf)
	end
end
