
# set up julia for mac12

/Applications/Julia-1.2.app/Contents/Resources/julia/bin/julia
using Distributed
no_procs = 1 # 4
addprocs( no_procs)
@everywhere project_folder = "/Users/nbonacker/Projects/01_inverse_problem/"
@everywhere cd( project_folder)



# use packages

@everywhere include( project_folder * "julia/simulation.jl")
@everywhere using .Simulation
@everywhere include( project_folder * "julia/network_inference.jl")
@everywhere using .NetworkInference
@everywhere using SharedArrays
include( project_folder * "julia/plot_results.jl")
using .PlotResults, JLD2, ColorSchemes



# define name

name = "inference_no_nodes=10_c=01_no_pert=10_no_samples=var_detailed"



# load results

output_jdl2 = string( "output/", name, "/results.jld")
@load( output_jdl2, x_axis, description, errors)
error_mean, error_std = get_errors_averaged( errors)



# plot reconstruction errors

x_label = "samples per perturbation"
y_label = "reconstruction error"
x_lim = ( 1, 28)
y_lim = ( 0.2, 0.8)
xscale = "linear"
yscale = "linear"
color_scheme = ColorSchemes.rainbow
colors = [ color_scheme[ 1/4], color_scheme[ 2/4], color_scheme[ 3/4], color_scheme[ 4/4]]
output_pdf = string( "output/", name, "/", name, ".pdf")
plot_error( x_axis, error_mean, error_std, colors, x_label, y_label, x_lim, y_lim, xscale, yscale, output_pdf)

output_pdf = string( "output/", name, "/", name, "*.pdf")
error_mean[ 3:4, :, :], error_std[ 3:4, :, :] = 0.9 * error_mean[ 3:4, :, :], 0.9 * error_std[ 3:4, :, :]
plot_error( x_axis, error_mean, error_std, colors, x_label, y_label, x_lim, y_lim, xscale, yscale, output_pdf)
