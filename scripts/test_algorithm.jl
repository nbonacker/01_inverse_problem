

# start or attach tmux session

tmux new -s 'inference_no_nodes=6_c=05_no_samples_per_pert=var'
tmux attach-session -t 'inference_no_nodes=6_c=05_no_samples_per_pert=var'



# set up julia for Niklas-ThinkPad-L13-Yoga

/home/niklas/Source/julia-1.4.1/bin/julia

using Distributed
no_procs = 4
addprocs( no_procs)
@everywhere project_folder = "/home/niklas/Projects/01_inverse_problem/"
@everywhere cd( project_folder)



# set up julia for mac12

/Applications/Julia-1.2.app/Contents/Resources/julia/bin/julia

using Distributed
no_procs = 4
addprocs( no_procs)
@everywhere project_folder = "/Users/nbonacker/Projects/01_inverse_problem/"
@everywhere cd( project_folder)



# make output direktory

name = "inference_no_nodes=6_c=05_no_samples_per_pert=var"
mkdir( string( project_folder * "output/", name))



# write script to file

script = open( "scripts/" * name * ".jl")
write( string( "output/", name, "/script.txt"), script)



# use packages

@everywhere include( project_folder * "julia/simulation.jl")
@everywhere using .Simulation
@everywhere include( project_folder * "julia/network_inference.jl")
@everywhere using .NetworkInference
@everywhere using SharedArrays
include( project_folder * "julia/plot_results.jl")
using .PlotResults
using JLD2, ColorSchemes



# setup variables

no_nodes = 6
no_pertubations = 6
no_drugs = 6
vector_no_samples_per_pertubation = [ 2, 4, 8, 16, 32, 64]
pertubation_type = "single"
matrix_type = "randn"
β = 0.5
a = ones( no_nodes)
b = ones( no_nodes)
c = 0.5 * ones( no_nodes)
σ = 0.0



# network reconstruction

no_run = 64
# no_run = 4

errors = SharedArray{Float64}( 6, length( vector_no_samples_per_pertubation), no_run)

@sync @distributed for run in 1:no_run

	# run = 1

	ω, θ, pert, x = simulate_data( no_nodes, no_pertubations, no_drugs, pertubation_type, matrix_type, β, a, b, c)

		for n in 1:length( vector_no_samples_per_pertubation)

			 # n = 2

			 no_samples_per_pertubation = vector_no_samples_per_pertubation[ n]
			 samples = get_samples( x, no_samples_per_pertubation)
			 ω_start = zeros( no_nodes, no_nodes)
			 search_space = Dict( "lb" => -1.0 * ones( no_nodes^2), "ub" => 1.0 * ones( no_nodes^2))

			 @time ω_GN_DIRECT_L = max_likelihood( pert, samples, θ, "gaussian theory", :GN_DIRECT_L, "flat prior", a, b, c, σ, 1e-2, 4e+4, vec( ω), search_space)
			 println( "ω_GN_DIRECT_L for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
			 println()
			 errors[ 1, n, run] = get_squared_error( ω, ω_GN_DIRECT_L)

			 @time ω_GN_MLSL = max_likelihood( pert, samples, θ, "gaussian theory", :GN_MLSL, "flat prior", a, b, c, σ, 1e-2, 4e+4, vec( ω), search_space)
			 println( "ω_GN_MLSL for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
			 println()
			 errors[ 2, n, run] = get_squared_error( ω, ω_GN_MLSL)

			 @time ω_GN_ISRES = max_likelihood( pert, samples, θ, "gaussian theory", :GN_ISRES, "flat prior", a, b, c, σ, 1e-2, 4e+4, vec( ω), search_space)
			 println( "ω_GN_ISRES for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
			 println()
			 errors[ 3, n, run] = get_squared_error( ω, ω_GN_ISRES)

			 @time ω_LN_BOBYQA = max_likelihood( pert, samples, θ, "gaussian theory", :LN_BOBYQA, "flat prior", a, b, c, σ, 1e-2, 4e+4, vec( ω), search_space)
			 println( "ω_LN_BOBYQA for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
			 println()
			 errors[ 4, n, run] = get_squared_error( ω, ω_LN_BOBYQA)

			 @time ω_LN_PRAXIS = max_likelihood( pert, samples, θ, "gaussian theory", :LN_PRAXIS, "flat prior", a, b, c, σ, 1e-2, 4e+4, vec( ω), search_space)
			 println( "ω_LN_PRAXIS for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
			 println()
			 errors[ 5, n, run] = get_squared_error( ω, ω_LN_PRAXIS)

			 @time ω_LN_SBPLX = max_likelihood( pert, samples, θ, "gaussian theory", :LN_SBPLX, "flat prior", a, b, c, σ, 1e-2, 4e+4, vec( ω), search_space)
			 println( "ω_LN_SBPLX for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
			 println()
			 errors[ 6, n, run] = get_squared_error( ω, ω_LN_SBPLX)
	end
end

# :GN_DIRECT_L
# :GN_CRS
# :GN_MLSL
# :GN_ISRES

# :LN_COBYQA
# :LN_BOBYQA
# :LN_PRAXIS
# :LN_SBPLX

# save results

x_axis = vector_no_samples_per_pertubation
description = [ "min squares first order", "min squares second order", "min squares gaussian theory", "max likelihood gaussian theory", "min squares gaussian theory*", "max likelihood gaussian theory*"]

output_jdl2 = string( "output/", name, "/results.jld")

@save( output_jdl2, x_axis, description, errors)



# load results

output_jdl2 = string( "output/", name, "/results.jld")
@load( output_jdl2, x_axis, description, errors)



# plot reconstruction errors

error_mean, error_std = get_errors_averaged( errors)
x_label = "samples per perturbation"
y_label = "reconstruction error"
x_lim = ( 1, 120)
y_lim = ( 0.0, 2.0)
xscale = "linear"
yscale = "linear"
color_scheme = ColorSchemes.rainbow
colors = [ color_scheme[ 1/4], color_scheme[ 2/4], color_scheme[ 3/4], color_scheme[ 4/4]]
output_pdf = string( "output/", name, "/error.pdf")

plot_error( x_axis, error_mean, errors_std, colors, x_label, y_label, x_lim, y_lim, xscale, yscale)



# detach or remove tmux session

Ctrl-b + d
