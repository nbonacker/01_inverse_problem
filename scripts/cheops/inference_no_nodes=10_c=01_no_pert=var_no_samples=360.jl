
# cd project folder

@everywhere project_folder = "/home/nbonacke/01_inverse_problem/"
@everywhere cd( project_folder)

# mkdir output

name = "inference_no_nodes=10_c=01_no_pert=var_no_samples=360"
println( "name: ", name)
mkpath( string( project_folder * "output/", name))

# write script to file

script = open( project_folder * "scripts/cheops/" * name * ".jl")
write( string( project_folder * "output/", name, "/script.txt"), script)

# use packages

@everywhere include( project_folder * "julia/simulation.jl")
@everywhere using .Simulation
@everywhere include( project_folder * "julia/network_inference.jl")
@everywhere using .NetworkInference
@everywhere using SharedArrays
using JLD2

# define variables

no_nodes = 10
no_drugs = 10
vector_no_samples_per_pertubation = [ 1, 2, 3, 4, 5, 6, 8, 9, 10]
no_samples = 360
pertubation_type = "randn"
matrix_type = "randn"
β = 0.5
a = 1.0 * ones( no_nodes)
b = 1.0 * ones( no_nodes)
c = 0.1 * ones( no_nodes)
σ = 0.0

# network reconstruction

no_run = 48 # 4 runs on 4 nodes take about 12:00 hours with 64GB mem 
errors = SharedArray{Float64}( 4, length( vector_no_samples_per_pertubation), no_run)

@sync @distributed for run in 1:no_run

  for n in 1:length( vector_no_samples_per_pertubation)

    no_pertubations = Int( no_samples / vector_no_samples_per_pertubation[ n])
    ω, θ, pert, x = simulate_data( no_nodes, no_pertubations, no_drugs, pertubation_type, matrix_type, β, a, b, c)

    no_samples_per_pertubation = vector_no_samples_per_pertubation[ n]
    samples = get_samples( x, no_samples_per_pertubation)
    ω_start = zeros( no_nodes, no_nodes)
    search_space = Dict( "lb" => -1.0 * ones( no_nodes^2), "ub" => 1.0 * ones( no_nodes^2))

    @time ω_ms_1o = min_squares( pert, samples, θ, "exact relations first order", :LN_BOBYQA, "flat prior", a, b, c, 1e-2, 2e+4, vec( ω_start), search_space)
    println( "ω_ms_1o for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
    println()
    errors[ 1, n, run] = get_squared_error( ω, ω_ms_1o)

    @time ω_ms_2o = min_squares( pert, samples, θ, "exact relations second order", :LN_BOBYQA, "flat prior", a, b, c, 1e-2, 2e+4, vec( ω_start), search_space)
    println( "ω_ms_2o for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
    println()
    errors[ 2, n, run] = get_squared_error( ω, ω_ms_2o)

    @time ω_ms_gt = min_squares( pert, samples, θ, "gaussian theory", :LN_BOBYQA, "flat prior", a, b, c, 1e-2, 2e+4, vec( ω_start), search_space)
    println( "ω_ms_gt for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
    println()
    errors[ 3, n, run] = get_squared_error( ω, ω_ms_gt)

    @time ω_ml_gt = max_likelihood( pert, samples, θ, "gaussian theory", :LN_BOBYQA, "flat prior", a, b, c, σ, 1e-2, 2e+4, vec( ω_start), search_space)
    println( "ω_ml_gt for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
    println()
    errors[ 4, n, run] = get_squared_error( ω, ω_ml_gt)
  end
end

# save results

x_axis = vector_no_samples_per_pertubation
description = [ "min squares first order", "min squares second order", "min squares gaussian theory", "max likelihood gaussian theory"]
output_jdl2 = string( "output/", name, "/results.jld")

@save( output_jdl2, x_axis, description, errors)
