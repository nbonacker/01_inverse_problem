#!/bin/bash -l
#SBATCH --job-name=inverse_problem
#SBATCH --output=/scratch/nbonacke/stdout/%j.out
#SBATCH --error=/scratch/nbonacke/stderr/%j.out
#SBATCH --mem=64GB
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH --time=1-12:00:00
#SBATCH --account=AG-Berg
#SBATCH --mail-user=nbonacke@uni-koeln.de
#SBATCH --mail-type=ALL

# use --ntasks-per-node=4
# use --time=24:00:00
# use --mem=16GB
# use -p4

# sbatch /home/nbonacke/01_inverse_problem/scripts/cheops/run.sh inference_no_nodes=10_c=01_no_pert=10_no_samples=var.jl
# sbatch /home/nbonacke/01_inverse_problem/scripts/cheops/run.sh inference_no_nodes=10_c=1_no_pert=10_no_samples=var.jl

# use --ntasks-per-node=8
# use --time=24:00:00
# use --mem=64GB
# use -p8

# sbatch /home/nbonacke/01_inverse_problem/scripts/cheops/run.sh inference_no_nodes=6_c=01_no_pert=var_no_samples=360.jl
# sbatch /home/nbonacke/01_inverse_problem/scripts/cheops/run.sh inference_no_nodes=6_c=1_no_pert=var_no_samples=360.jl

# use --ntasks-per-node=8
# use --time=7-00:00:00
# use --mem=128GB
# use -p8

# sbatch /home/nbonacke/01_inverse_problem/scripts/cheops/run.sh inference_no_nodes=10_c=01_no_pert=var_no_samples=360.jl

# use --ntasks-per-node=4
# use --time=1-12:00:00
# use --mem=64GB
# use -p4

# sbatch /home/nbonacke/01_inverse_problem/scripts/cheops/run.sh inference_no_nodes=10_c=05_no_pert=var_no_samples=360.jl


script_name=$1

module load curl/7.64.1 arpack/arpack96

/home/nbonacke/julia-1.4.2/bin/julia -p4 /home/nbonacke/01_inverse_problem/scripts/cheops/${script_name}
