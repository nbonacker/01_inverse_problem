#!/bin/bash -l
#SBATCH --job-name=inverse_problem
#SBATCH --output=/scratch/nbonacke/stdout/%j.out
#SBATCH --error=/scratch/nbonacke/stderr/%j.out
#SBATCH --mem=64GB
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH --time=2-00:00:00
#SBATCH --account=AG-Berg
#SBATCH --mail-user=nbonacke@uni-koeln.de
#SBATCH --mail-type=ALL

# sbatch /home/nbonacke/01_inverse_problem/scripts/cheops/run_inference_no_nodes=10_c=01_no_pert=10_no_samples=var_detailed.sh inference_no_nodes=10_c=01_no_pert=10_no_samples=var_detailed.jl

script_name=$1

module load curl/7.64.1 arpack/arpack96

/home/nbonacke/julia-1.4.2/bin/julia -p4 /home/nbonacke/01_inverse_problem/scripts/cheops/${script_name}
