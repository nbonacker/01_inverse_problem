
# start or attach tmux session

tmux new -s 'inference_no_nodes_10_c=01_no_pert=var_no_samples=360'
tmux attach-session -t 'inference_no_nodes_10_c=01_no_pert=var_no_samples=360'



# set up julia for Niklas-ThinkPad-L13-Yoga

/home/niklas/Source/julia-1.4.1/bin/julia
using Distributed
no_procs = 1 # 4
addprocs( no_procs)
@everywhere project_folder = "/home/niklas/Projects/01_inverse_problem/"
@everywhere cd( project_folder)



# set up julia for mac12

/Applications/Julia-1.2.app/Contents/Resources/julia/bin/julia
using Distributed
no_procs = 1 # 4
addprocs( no_procs)
@everywhere project_folder = "/Users/nbonacker/Projects/01_inverse_problem/"
@everywhere cd( project_folder)



# load name and make dir

name = "inference_no_nodes=10_c=01_no_pert=var_no_samples=360"
mkpath( string( project_folder * "output/", name))



# write script to file

script = open( "scripts/" * name * ".jl")
write( string( "output/", name, "/script.txt"), script)



# use packages

@everywhere include( project_folder * "julia/simulation.jl")
@everywhere using .Simulation
@everywhere include( project_folder * "julia/network_inference.jl")
@everywhere using .NetworkInference
@everywhere using SharedArrays
using Revise
includet( project_folder * "julia/plot_results.jl")
using .PlotResults, JLD2, ColorSchemes



# setup variables

no_nodes = 10
no_drugs = 10
vector_no_samples_per_pertubation = [ 1, 2, 3, 4, 5, 6, 8, 9, 10]
no_samples = 360
pertubation_type = "randn"
matrix_type = "randn"
β = 0.5
a = 1.0 * ones( no_nodes)
b = 1.0 * ones( no_nodes)
c = 0.1 * ones( no_nodes)
σ = 0.0



# network reconstruction

no_run = 1 # 64

errors = SharedArray{Float64}( 4, length( vector_no_samples_per_pertubation), no_run)

@sync @distributed for run in 1:no_run

  # run = 1

  for n in 1:length( vector_no_samples_per_pertubation)

	  # n = 2

    no_pertubations = Int( no_samples / vector_no_samples_per_pertubation[ n])

    ω, θ, pert, x = simulate_data( no_nodes, no_pertubations, no_drugs, pertubation_type, matrix_type, β, a, b, c)

    no_samples_per_pertubation = vector_no_samples_per_pertubation[ n]
    samples = get_samples( x, no_samples_per_pertubation)
    ω_start = zeros( no_nodes, no_nodes)
    search_space = Dict( "lb" => -1.0 * ones( no_nodes^2), "ub" => 1.0 * ones( no_nodes^2))

    @time ω_ms_1o = min_squares( pert, samples, θ, "exact relations first order", :LN_BOBYQA, "flat prior", a, b, c, 1e-2, 2e+4, vec( ω_start), search_space)
    println( "ω_ms_1o for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
    println()
    errors[ 1, n, run] = get_squared_error( ω, ω_ms_1o)

    @time ω_ms_2o = min_squares( pert, samples, θ, "exact relations second order", :LN_BOBYQA, "flat prior", a, b, c, 1e-2, 2e+4, vec( ω_start), search_space)
    println( "ω_ms_2o for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
    println()
    errors[ 2, n, run] = get_squared_error( ω, ω_ms_2o)

    @time ω_ms_gt = min_squares( pert, samples, θ, "gaussian theory", :LN_BOBYQA, "flat prior", a, b, c, 1e-2, 2e+4, vec( ω_start), search_space)
    println( "ω_ms_gt for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
    println()
    errors[ 3, n, run] = get_squared_error( ω, ω_ms_gt)

    @time ω_ml_gt = max_likelihood( pert, samples, θ, "gaussian theory", :LN_BOBYQA, "flat prior", a, b, c, σ, 1e-2, 2e+4, vec( ω_start), search_space)
    println( "ω_ml_gt for " * string( no_samples_per_pertubation) * " samples per pertubation done!")
    println()
    errors[ 4, n, run] = get_squared_error( ω, ω_ml_gt)
  end
end



# save results

x_axis = vector_no_samples_per_pertubation
# description = [ "min squares first order", "min squares second order", "min squares gaussian theory", "max likelihood gaussian theory", "min squares gaussian theory*", "max likelihood gaussian theory*"]
description = [ "min squares first order", "min squares second order", "min squares gaussian theory", "max likelihood gaussian theory"]
output_jdl2 = string( "output/", name, "/results.jld")

@save( output_jdl2, x_axis, description, errors)



# load results

output_jdl2 = string( "output/", name, "/results.jld")
@load( output_jdl2, x_axis, description, errors)



# plot reconstruction errors

error_mean, error_std = get_errors_averaged( errors)
x_label = "samples per perturbation"
y_label = "reconstruction error"
x_lim = ( 1, 10)
y_lim = ( 0.1, 0.4)
xscale = "linear"
yscale = "linear"
color_scheme = ColorSchemes.rainbow
colors = [ color_scheme[ 1/4], color_scheme[ 2/4], color_scheme[ 3/4], color_scheme[ 4/4]]
output_pdf = string( "output/", name, "/", name, ".pdf")
plot_error( x_axis, error_mean, error_std, colors, x_label, y_label, x_lim, y_lim, xscale, yscale, output_pdf)

error_mean[ 3:4, : , :], error_std[ 3:4, : , :] = 0.9 * error_mean[ 3:4, : , :], 0.9 * error_std[ 3:4, : , :]
output_pdf = string( "output/", name, "/", name, "*.pdf")
plot_error( x_axis, error_mean, error_std, colors, x_label, y_label, x_lim, y_lim, xscale, yscale, output_pdf)

# detach or remove tmux session

Ctrl-b + d
