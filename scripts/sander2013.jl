
# start or attach tmux session

tmux new -s 'sander_2013'
tmux new -s 'sander_2013_01'
tmux new -s 'sander_2013_02'
tmux new -s 'sander_2013_03'
tmux new -s 'sander_2013_04'

tmux attach-session -t 'sander_2013_04'



# set up julia for mac12

/Applications/Julia-1.2.app/Contents/Resources/julia/bin/julia
project_folder = "/Users/nbonacker/Projects/01_inverse_problem/"
cd( project_folder)



# set up julia for Niklas-ThinkPad-L13-Yoga

/home/niklas/Source/julia-1.4.1/bin/julia
project_folder = "/home/niklas/Projects/01_inverse_problem/"
cd( project_folder)



# define name and make dir

name = "sander_2013"
data_folder = project_folder * "data/Sander2013/DataSet-S1/"
output_folder = project_folder * "output/" * name * '/'
plots_folder = project_folder * "plots/" * name * '/'
mkpath( output_folder)
mkpath( plots_folder)



# use packages

using Revise, SharedArrays, DelimitedFiles, Statistics, ColorSchemes, Distributions, StatsBase
includet( project_folder * "julia/simulation.jl")
using .Simulation
includet( project_folder * "julia/network_inference.jl")
using .NetworkInference
includet( project_folder * "julia/plot_results.jl")
using .PlotResults
includet( project_folder * "julia/gaussian_theory.jl")
using .GaussianTheory



# load and rescale data

data = readdlm( data_folder * "data.txt")
rescale_data_sander_2013!( data)
input = readdlm( data_folder * "input.txt")
name = readdlm( data_folder * "name.txt")
pert = readdlm( data_folder * "pert.txt")
no_nodes = size( data)[ 1]



# devide data into trainig sets

list_predictions_01 = [ 1, 11, 21, 31, 41]
data_training_01, data_prediction_01 = devide_data( data, list_predictions_01)
pert_training_01, pert_prediction_01 = devide_data( pert, list_predictions_01)

list_predictions_02 = [ 2, 12, 22, 32, 42]
data_training_02, data_prediction_02 = devide_data( data, list_predictions_02)
pert_training_02, pert_prediction_02 = devide_data( pert, list_predictions_02)

list_predictions_03 = [ 3, 13, 23, 33, 43]
data_training_03, data_prediction_03 = devide_data( data, list_predictions_03)
pert_training_03, pert_prediction_03 = devide_data( pert, list_predictions_03)

list_predictions_04 = [ 4, 14, 24, 34, 44]
data_training_04, data_prediction_04 = devide_data( data, list_predictions_04)
pert_training_04, pert_prediction_04 = devide_data( pert, list_predictions_04)

output_ω_mlGt_training_01 = output_folder * "ω_mlGt_training_01"
output_ω_mlGt_training_02 = output_folder * "ω_mlGt_training_02"
output_ω_mlGt_training_03 = output_folder * "ω_mlGt_training_03"
output_ω_mlGt_training_04 = output_folder * "ω_mlGt_training_04"

output_ω_ms1o_training_01 = output_folder * "ω_ms1o_training_01"
output_ω_ms1o_training_02 = output_folder * "ω_ms1o_training_02"
output_ω_ms1o_training_03 = output_folder * "ω_ms1o_training_03"
output_ω_ms1o_training_04 = output_folder * "ω_ms1o_training_04"

# infere network based on training sets

ln_opt_method = :LN_BOBYQA
x_tolerance = 1e-1
max_evaluations = 1e+5

ω_lb, ω_ub = get_ω_bounds_sander_2013()
a_lb, a_ub = ones( no_nodes), ones( no_nodes)
b_lb, b_ub = ones( no_nodes), ones( no_nodes)
c_lb, c_ub = ones( no_nodes), ones( no_nodes)
θ_lb, θ_ub = zeros( no_nodes), zeros( no_nodes)
search_space = Dict( "lb" => concatenate( [ ω_lb, a_lb, b_lb, c_lb, θ_lb]), "ub" => concatenate( [ ω_ub, a_ub, b_ub, c_ub, θ_ub]))
ω_start = zeros( no_nodes^2)
a_start = ones( no_nodes)
b_start = ones( no_nodes)
c_start = ones( no_nodes)
θ_start = zeros( no_nodes)
y_start = concatenate( [ ω_start, a_start, b_start, c_start, θ_start])

prior = ( "gaussian", 0.0, 0.1)
σ = 0.0
no_samples_per_pertubation = 1

# 01

no_nodes = size( data_training_01)[ 1]
no_perturbations = size( data_training_01)[ 2]
samples_01 = [ Array{Float64}( undef, no_nodes, no_samples_per_pertubation) for i in 1:no_perturbations]

for μ in 1:no_perturbations

	samples_01[ μ][ :, 1] = data_training_01[ :, μ]
end

ω_mlGt_training_01, a_mlGt_training_01, b_mlGt_training_01, c_mlGt_training_01, θ_mlGt_training_01 = max_likelihood( pert_training_01, samples_01, "gaussian theory", ln_opt_method, prior, σ, x_tolerance, max_evaluations, y_start, search_space)
open( output_ω_mlGt_training_01, "w") do io
	writedlm( io, ω_mlGt_training_01)
end
ω_ms1o_training_01, a_ms1o_training_01, b_ms1o_training_01, c_ms1o_training_01, θ_ms1o_training_01 = min_squares( pert_training_01, samples_01, "exact relations first order", ln_opt_method, "flat prior", x_tolerance, max_evaluations, y_start, search_space)
open( output_ω_ms1o_training_01, "w") do io
	writedlm( io, ω_ms1o_training_01)
end

# 02

no_nodes = size( data_training_02)[ 1]
no_perturbations = size( data_training_02)[ 2]
samples_02 = [ Array{Float64}( undef, no_nodes, no_samples_per_pertubation) for i in 1:no_perturbations]

for μ in 1:no_perturbations

	samples_02[ μ][ :, 1] = data_training_02[ :, μ]
end

ω_mlGt_training_02, a_mlGt_training_02, b_mlGt_training_02, c_mlGt_training_02, θ_mlGt_training_02 = max_likelihood( pert_training_02, samples_02, "gaussian theory", ln_opt_method, prior, σ, x_tolerance, max_evaluations, y_start, search_space)
open( output_ω_mlGt_training_02, "w") do io
	writedlm( io, ω_mlGt_training_02)
end
ω_ms1o_training_02, a_ms1o_training_02, b_ms1o_training_02, c_ms1o_training_02, θ_ms1o_training_02 = min_squares( pert_training_02, samples_02, "exact relations first order", ln_opt_method, "flat prior", x_tolerance, max_evaluations, y_start, search_space)
open( output_ω_ms1o_training_02, "w") do io
	writedlm( io, ω_ms1o_training_02)
end

# 03

no_nodes = size( data_training_03)[ 1]
no_perturbations = size( data_training_03)[ 2]
samples_03 = [ Array{Float64}( undef, no_nodes, no_samples_per_pertubation) for i in 1:no_perturbations]

for μ in 1:no_perturbations

	samples_03[ μ][ :, 1] = data_training_03[ :, μ]
end

ω_mlGt_training_03, a_ms1o_training_03, b_ms1o_training_03, c_ms1o_training_03, θ_ms1o_training_03 = max_likelihood( pert_training_03, samples_03, "gaussian theory", ln_opt_method, prior, σ, x_tolerance, max_evaluations, y_start, search_space)
open( output_ω_mlGt_training_03, "w") do io
	writedlm( io, ω_mlGt_training_03)
end
ω_ms1o_training_03, a_ms1o_training_03, b_ms1o_training_03, c_ms1o_training_03, θ_ms1o_training_03 = min_squares( pert_training_03, samples_03, "exact relations first order", ln_opt_method, "flat prior", x_tolerance, max_evaluations, y_start, search_space)
open( output_ω_ms1o_training_03, "w") do io
	writedlm( io, ω_ms1o_training_03)
end

# 04

no_nodes = size( data_training_04)[ 1]
no_perturbations = size( data_training_04)[ 2]
samples_04 = [ Array{Float64}( undef, no_nodes, no_samples_per_pertubation) for i in 1:no_perturbations]

for μ in 1:no_perturbations

	samples_04[ μ][ :, 1] = data_training_04[ :, μ]
end

ω_mlGt_training_04, a_ms1o_training_04, b_ms1o_training_04, c_ms1o_training_04, θ_ms1o_training_04 = max_likelihood( pert_training_04, samples_04, "gaussian theory", ln_opt_method, prior, σ, x_tolerance, max_evaluations, y_start, search_space)
open( output_ω_mlGt_training_04, "w") do io
	writedlm( io, ω_mlGt_training_04)
end
ω_ms1o_training_04, a_ms1o_training_04, b_ms1o_training_04, c_ms1o_training_04, θ_ms1o_training_04 = min_squares( pert_training_04, samples_04, "exact relations first order", ln_opt_method, "flat prior", x_tolerance, max_evaluations, y_start, search_space)
open( output_ω_ms1o_training_04, "w") do io
	writedlm( io, ω_ms1o_training_04)
end

# predict expression based on infered networcs‚

ω_mlGt_training_01 = readdlm( output_ω_mlGt_training_01)
ω_mlGt_training_02 = readdlm( output_ω_mlGt_training_02)
ω_mlGt_training_03 = readdlm( output_ω_mlGt_training_03)
ω_mlGt_training_04 = readdlm( output_ω_mlGt_training_04)

ω_ms1o_training_01 = readdlm( output_ω_ms1o_training_01)
ω_ms1o_training_02 = readdlm( output_ω_ms1o_training_02)
ω_ms1o_training_03 = readdlm( output_ω_ms1o_training_03)
ω_ms1o_training_04 = readdlm( output_ω_ms1o_training_04)

no_training_perturbations = size( data_training_01)[ 2]

u_training_01 = Array{ Float64, 2}( undef, no_nodes, no_training_perturbations)
u_training_02 = Array{ Float64, 2}( undef, no_nodes, no_training_perturbations)
u_training_03 = Array{ Float64, 2}( undef, no_nodes, no_training_perturbations)
u_training_04 = Array{ Float64, 2}( undef, no_nodes, no_training_perturbations)
m_mlGt_training_01 = Array{ Float64, 2}( undef, no_nodes, no_training_perturbations)
m_mlGt_training_02 = Array{ Float64, 2}( undef, no_nodes, no_training_perturbations)
m_mlGt_training_03 = Array{ Float64, 2}( undef, no_nodes, no_training_perturbations)
m_mlGt_training_04 = Array{ Float64, 2}( undef, no_nodes, no_training_perturbations)
m_ms1o_training_01 = Array{ Float64, 2}( undef, no_nodes, no_training_perturbations)
m_ms1o_training_02 = Array{ Float64, 2}( undef, no_nodes, no_training_perturbations)
m_ms1o_training_03 = Array{ Float64, 2}( undef, no_nodes, no_training_perturbations)
m_ms1o_training_04 = Array{ Float64, 2}( undef, no_nodes, no_training_perturbations)

for μ in 1:no_training_perturbations

  u_training_01[ :, μ] = pert_training_01[ :, μ] + θ
  m_mlGt_training_01[ :, μ], foo = get_mχ_gaussian_theory( ω_mlGt_training_01, u_training_01[ :, μ], a, b, c)
  m_ms1o_training_01[ :, μ], foo = get_mχ_gaussian_theory( ω_ms1o_training_01, u_training_01[ :, μ], a, b, c)
  u_training_02[ :, μ] = pert_training_02[ :, μ] + θ
  m_mlGt_training_02[ :, μ], foo = get_mχ_gaussian_theory( ω_mlGt_training_02, u_training_02[ :, μ], a, b, c)
  m_ms1o_training_02[ :, μ], foo = get_mχ_gaussian_theory( ω_ms1o_training_02, u_training_02[ :, μ], a, b, c)
  u_training_03[ :, μ] = pert_training_03[ :, μ] + θ
  m_mlGt_training_03[ :, μ], foo = get_mχ_gaussian_theory( ω_mlGt_training_03, u_training_03[ :, μ], a, b, c)
  m_ms1o_training_03[ :, μ], foo = get_mχ_gaussian_theory( ω_ms1o_training_03, u_training_03[ :, μ], a, b, c)
  u_training_04[ :, μ] = pert_training_04[ :, μ] + θ
  m_mlGt_training_04[ :, μ], foo = get_mχ_gaussian_theory( ω_mlGt_training_04, u_training_04[ :, μ], a, b, c)
  m_ms1o_training_04[ :, μ], foo = get_mχ_gaussian_theory( ω_ms1o_training_04, u_training_04[ :, μ], a, b, c)
end

x_label = "measured gene expression"
y_label = "predicted gene expression"
x_lim = [ -1, 1]
y_lim = [ -1, 1]
color_scheme = ColorSchemes.rainbow
color_list = [ color_scheme[ 1/4], color_scheme[ 4/4]]

output_pdf_training_01 = plots_folder * "scatterplot_training_01.pdf"
scatter_forward_problem( data_training_01, [ m_ms1o_training_01, m_mlGt_training_01], x_label, y_label, x_lim, y_lim, color_list, output_pdf_training_01)

output_pdf_training_02 = plots_folder * "scatterplot_training_02.pdf"
scatter_forward_problem( data_training_02, [ m_ms1o_training_02, m_mlGt_training_02], x_label, y_label, x_lim, y_lim, color_list, output_pdf_training_02)

output_pdf_training_03 = plots_folder * "scatterplot_training_03.pdf"
scatter_forward_problem( data_training_03, [ m_ms1o_training_03, m_mlGt_training_03], x_label, y_label, x_lim, y_lim, color_list, output_pdf_training_03)

output_pdf_training_04 = plots_folder * "scatterplot_training_04.pdf"
scatter_forward_problem( data_training_04, [ m_ms1o_training_04, m_mlGt_training_04], x_label, y_label, x_lim, y_lim, color_list, output_pdf_training_04)



# reconstruct network

no_nodes = size( data)[ 1]
no_perturbations = size( data)[ 2]
no_samples_per_pertubation = 1

samples = [ Array{Float64}( undef, no_nodes, no_samples_per_pertubation) for i in 1:no_perturbations]

for μ in 1:no_perturbations

	samples[ μ][ :, 1] = data[ :, μ]
end

a = 1.0 * ones( no_nodes)
b = 1.0 * ones( no_nodes)
c = 0.1 * ones( no_nodes)
θ = 0.0 * ones( no_nodes)

ln_opt_method = :LN_BOBYQA
prior = "flat prior"
x_tolerance = 1e-1
max_evaluations = 1e+5
ω_start = zeros( no_nodes, no_nodes)
search_space_sander_2013 = get_search_space_sander_2013()

ω_ms_1o = min_squares( pert, samples, θ, "exact relations first order", ln_opt_method, prior, a, b, c, x_tolerance, max_evaluations, vec( ω_start), search_space_sander_2013)
# ω_ms_1o = min_squares( pert_training_01, samples_01, θ, "exact relations first order", ln_opt_method, prior, a, b, c, x_tolerance, max_evaluations, vec( ω_start), search_space_sander_2013)
output_ω_ms_1o = output_folder * "ω_ms_1o.txt"
open( output_ω_ms_1o, "w") do io
	writedlm( io, ω_ms_1o)
end

ω_ms_2o = min_squares( pert, samples, θ, "exact relations second order", ln_opt_method, prior, a, b, c, x_tolerance, max_evaluations, vec( ω_start), search_space_sander_2013)
output_ω_ms_2o = output_folder * "ω_ms_2o.txt"
open( output_ω_ms_2o, "w") do io
	writedlm( io, ω_ms_2o)
end

prior = ( "gaussian", 0.0, 0.1)
σ = 0.0
ω_ml_gt_prior_gaussian_00_01 = max_likelihood( pert, samples, θ, "gaussian theory", ln_opt_method, prior, a, b, c, σ, x_tolerance, max_evaluations, vec( ω_start), search_space_sander_2013)
output_ω_ml_gt_prior_gaussian_00_01 = output_folder * "ω_ml_gt_prior_gaussian_00_01"
open( output_ω_ml_gt_prior_gaussian_00_01, "w") do io
	writedlm( io, ω_ml_gt_prior_gaussian_00_01)
end

prior = ( "gaussian", 0.0, 0.2)
σ = 0.0
ω_ml_gt_prior_gaussian_00_02 = max_likelihood( pert, samples, θ, "gaussian theory", ln_opt_method, prior, a, b, c, σ, x_tolerance, max_evaluations, vec( ω_start), search_space_sander_2013)
output_ω_ml_gt_prior_gaussian_00_02 = output_folder * "ω_ml_gt_prior_gaussian_00_02"
open( output_ω_ml_gt_prior_gaussian_00_02, "w") do io
	writedlm( io, ω_ml_gt_prior_gaussian_00_02)
end

prior = ( "gaussian", 0.0, 0.4)
σ = 0.0
ω_ml_gt_prior_gaussian_00_04 = max_likelihood( pert, samples, θ, "gaussian theory", ln_opt_method, prior, a, b, c, σ, x_tolerance, max_evaluations, vec( ω_start), search_space_sander_2013)
output_ω_ml_gt_prior_gaussian_00_04 = output_folder * "ω_ml_gt_prior_gaussian_00_04"
open( output_ω_ml_gt_prior_gaussian_00_04, "w") do io
	writedlm( io, ω_ml_gt_prior_gaussian_00_04)
end



# prepare the network for visualization

ω_ml_gt_prior_gaussian_00_04 = readdlm( output_folder * "ω_ml_gt_prior_gaussian_00_04")
δ = 0.2
ω_filtered = get_ω_filtered( ω_ml_gt_prior_gaussian_00_04, δ)
foo = ω_filtered
list_to_truncate = [ 20, 14, 13, 12, 11, 10, 9, 8] # for 00_04 be awrae of change in index, therefore start with largest index
for k in list_to_truncate

	global foo = get_ω_truncated( foo, k)
end
ω_truncated = foo

name = readdlm( data_folder * "name.txt")
name_final = readdlm( data_folder * "name_final.txt")



# using TikzGraphs, TikzPictures, LightGraphs

using Pkg, TikzPictures, LightGraphs
Pkg.activate( "MyTikzGraphs")
using MyTikzGraphs

no_nodes = size( ω_truncated)[ 1]
no_measured_nodes = 10

g = DiGraph( no_nodes)
dict_edge_styles = Dict()
dict_node_styles = Dict()

for i in 1:no_measured_nodes

	merge!( dict_node_styles,	Dict( i => "circle"))
end

for i in no_measured_nodes + 1 : no_nodes

	merge!( dict_node_styles, Dict( i => "rounded corners"))
end

for i in 1:no_nodes, j in 1:no_nodes

	if ω_truncated[ j, i] > 0

		add_edge!( g, i, j)
		merge!( dict_edge_styles, Dict( ( i, j) => "mygreen"))
	elseif ω_truncated[ j, i] < 0

		add_edge!( g, i, j)
		merge!( dict_edge_styles, Dict( ( i, j) => "myred"))
	end
end

output_pdf = output_folder * "tikz_gene_interaction_graph.pdf"
list_names = String.( name_final[ :, 2])
# t = MyTikzGraphs.plot( g, Layouts.SimpleNecklace(), list_names, node_style = "draw", node_styles = dict_node_styles, edge_styles = dict_edge_styles)
t = MyTikzGraphs.plot( g, Layouts.SimpleNecklace(), list_names, node_style = "draw", node_styles = dict_node_styles, edge_style = "thick", edge_styles = dict_edge_styles)
TikzPictures.save( PDF( output_pdf), t)
